#pragma once

#include "textrenderer.h"
#include "tgmath/Vec2.h"
#include "bones.h"

#include <SDL2/SDL.h>

#include <set>
#include <string>
#include <vector>
#include <functional>

namespace tG
{
	namespace UI
	{
		void init(); // CALL THIS!

		class I_GUIMouse;
		class I_GUIKeyboard;
		class I_Renderable;
		class ConstDropDown;
		class AnimationTimeline;

		// static Variables
		//---
		extern Font symbols_font;
		extern Font default_font;
		extern SDL_Color default_text_color,
		                 default_marker_color,
		                 default_marker_border_color,
		                 default_new_button_out_color;
		extern ColorRGBA default_border_color,
		                 default_bg_color,
		                 default_new_button_coss_color,
		                 default_shadow_color;
		extern int default_border;
		extern const std::string SYMBOL_ARROW_DOWN;
		extern const std::string SYMBOL_ARROW_UP;
		extern const std::string SYMBOL_ARROW_LEFT;
		extern const std::string SYMBOL_MARKER;
		extern const std::string SYMBOL_NEW;

		extern std::vector<I_GUIMouse*> all_GUIMouse;
		extern std::vector<I_GUIKeyboard*> all_GUIKeyboard;
		extern std::vector<I_Renderable*> all_renderables;
		// TODO space partition DS
		// TODO layers

		using MouseButton = int;
		const MouseButton LEFT_MB = 0x0;
		const MouseButton RIGHT_MB = 0x1;
		const MouseButton MIDDLE_MB = 0x2;
		const MouseButton _4_MB = 0x3;
		const MouseButton _5_MB = 0x4;

		// Static Functions
		//---
		void render();
		void registerGUIMouse(I_GUIMouse* component);
		void registerGUIKeyboard(I_GUIKeyboard* component);
		void registerRenderable(I_Renderable* component);
		// Mouse positioning is Carthesian !!!
		bool mouseMovedInGUI(const TGMath::i::Vec2& m_pos, const TGMath::i::Vec2& m_mov); /// returns true if the movement was processed by the GUI, false if it has to be processed by the game
		bool mouseButtonInGUI(const TGMath::i::Vec2& m_pos, MouseButton which, bool up); /// returns true if the movement was processed by the GUI, false if it has to be processed by the game
		bool mouseWheelInGUI(const TGMath::i::Vec2& m_pos, bool up); /// returns true if the movement was processed by the GUI, false if it has to be processed by the game
		bool keyPressed(int SDL_key, const Uint8* keys);
		bool keyReleased(int SDL_key, const Uint8* keys);

		// TODO joypad management

		class I_GUIMouse
		{
			// Mouse positioning is Carthesian !!!
		public:
			I_GUIMouse();
			virtual bool isMouseOnMe(const TGMath::i::Vec2& m_pos) = 0;
			virtual bool mouseMovedInGUI(const TGMath::i::Vec2& m_pos, const TGMath::i::Vec2& m_mov) = 0; /// returns true if the movement was processed by the GUI, false if it has to be processed by the game
			virtual bool mouseButtonInGUI(const TGMath::i::Vec2& m_pos, MouseButton which, bool up) = 0; /// returns true if the movement was processed by the GUI, false if it has to be processed by the game
			virtual bool mouseWheelInGUI(const TGMath::i::Vec2& m_pos, bool up) = 0; /// returns true if the movement was processed by the GUI, false if it has to be processed by the game
		};

		class I_GUIKeyboard
		{
		public:
			I_GUIKeyboard();

			virtual bool keyPressed(int SDL_key, const Uint8* keys) = 0;
			virtual bool keyReleased(int SDL_key, const Uint8* keys) = 0;
		};

		class I_Renderable
		{
		public:
			I_Renderable();

			virtual void render() = 0;
		};


		using Rectangle = struct Rectangle_st
		{
			TGMath::i::Vec2 pos_tl;
			int width, height;

			bool isPointInside(const TGMath::i::Vec2& p) const;
		};


		class ConstDropDown : I_GUIMouse, I_Renderable
		{
		protected:
			int width, height;
			int max_width, max_height; // -1 means unlimited
			int min_width, min_height; // 0 means unlimited

			uint8_t flags;

			std::vector<SDLWrap_Texture> text_textures;
			std::vector<const std::string*> text_strings;
			uint selected;
			bool is_open;
			int rbb_w,
			     rbb_h;

			int whichLine(int y_carth) const;
			int mouse_over_line;
			int line_h;

			void updateSize();
			void select(int id);
		public:
			// Flags
			//---
			static const uint8_t FIXED_WIDTH  = 0x01;	// width is fixed
			static const uint8_t FIXED_HEIGHT = 0x01<<1; // height is fixed
			static const uint8_t FIXED_SIZE   = FIXED_WIDTH | FIXED_HEIGHT; // width and height are fixed

			const std::set<std::string>& strings;

			ConstDropDown(const std::set<std::string>& strings);

			TGMath::i::Vec2 pos; /// top left corner
			std::function<void (uint selected, uint old_selected, const std::vector<const std::string*>& strings)> onSelectCallback;

			void setFlag(uint8_t flag);
			void unsetFlag(uint8_t flag);
			bool hasFlag(uint8_t flag) const;

			void setWidth(uint width);
			void setHeight(uint height);
			uint getWidth() const;
			uint getHeight() const;
			void setMinWidth(uint min_width);
			void setMinHeight(uint min_height);
			uint getMinWidth() const;
			uint getMinHeight() const;
			void setMaxWidth(uint max_width);
			void setMaxHeight(uint max_height);
			uint getMaxWidth() const;
			uint getMaxHeight() const;

			bool isClosed() const;
			bool isOpen() const;
			void open();
			void close();

			// I_Renderable
			//---
			void render();

			// I_GUIMouse
			//---
			virtual bool isMouseOnMe(const TGMath::i::Vec2& m_pos);
			virtual bool mouseMovedInGUI(const TGMath::i::Vec2& m_pos, const TGMath::i::Vec2& m_mov); /// returns true if the movement was processed by the GUI, false if it has to be processed by the game
			virtual bool mouseButtonInGUI(const TGMath::i::Vec2& m_pos, MouseButton which, bool up); /// returns true if the movement was processed by the GUI, false if it has to be processed by the game
			virtual bool mouseWheelInGUI(const TGMath::i::Vec2& m_pos, bool up); /// returns true if the movement was processed by the GUI, false if it has to be processed by the game
		}; // ConstDropDown

		class AnimationTimeline : I_GUIMouse, I_GUIKeyboard, I_Renderable
		{
		private:
			using Marker = struct Marker_st
			{
				Rectangle AABB;
				std::shared_ptr<Bone::Keyframe> keyframe;
			};

			Skeleton* skeleton;
			const std::string* active_animation;

			const static ColorRGBA notches_color;

			std::vector<SDLWrap_Texture> names_textures;
			int names_max_w;

			Rectangle AABB;
			int line_h;

			int whichLine(int y_carth) const;
			int mouse_over_line;

			std::vector< std::vector<Marker> > lines;
			int selected_bone;
			std::shared_ptr<Bone::Keyframe> selected_keyframe;

			bool animating;
			Time game_time;

			int pxps; // TODO make scalable

			static Rectangle new_keyframe_button_AABB;

		protected:
			// methods
			void selectKeyframe(std::shared_ptr<Bone::Keyframe> keyframe);

		public:
			static void init(); // called by UI::init();
			AnimationTimeline();
			AnimationTimeline(Skeleton* skeleton);

			// attributes
			std::function<void (std::shared_ptr<Bone::Keyframe> keyframe)> onSelectKeyframeCallback;


			bool setAnimation(const std::string& animamtion_name);
			void setSkeleton(Skeleton* skeleton);
			Skeleton* getSkeleton();
			const std::string& getActiveAnimation() const;
			bool isAnimating() const;
			void selectBone(Bone* bone);
			void selectBone(int bone_id);

			void update(Time game_time); // called once per game loop iteration 

			// I_Renderable
			//---
			void render();

			// I_GUIMouse
			//---
			virtual bool isMouseOnMe(const TGMath::i::Vec2& m_pos);
			virtual bool mouseMovedInGUI(const TGMath::i::Vec2& m_pos, const TGMath::i::Vec2& m_mov); /// returns true if the movement was processed by the GUI, false if it has to be processed by the game
			virtual bool mouseButtonInGUI(const TGMath::i::Vec2& m_pos, MouseButton which, bool up); /// returns true if the movement was processed by the GUI, false if it has to be processed by the game
			virtual bool mouseWheelInGUI(const TGMath::i::Vec2& m_pos, bool up); /// returns true if the movement was processed by the GUI, false if it has to be processed by the game

			// I_GUIKeyboard
			//---
			virtual bool keyPressed(int SDL_key, const Uint8* keys);
			virtual bool keyReleased(int SDL_key, const Uint8* keys);

		}; // AnimationTimeline
	}; // namespace UI
}; // namespace tG