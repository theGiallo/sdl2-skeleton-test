#pragma once

#include <sstream>

/**
 * code from
 * https://stackoverflow.com/questions/303562/c-format-macro-inline-ostringstream/303716#303716
 **/

class MakeString
{
public:
	std::ostringstream stream;
	operator std::string() const { return stream.str(); }

	template<class T>
	MakeString& operator<<(T const& VAR) { stream << VAR; return *this; }
};

class MakeStringSingleInstance
{
public:
	static std::ostringstream stream;
	operator std::string() const { std::string s = stream.str(); stream.str(""); return s;}

	template<class T>
	MakeStringSingleInstance& operator<<(T const& VAR) { stream << VAR; return *this; }

	static MakeStringSingleInstance instance;
};

namespace tG
{
	extern MakeStringSingleInstance& ms;
};