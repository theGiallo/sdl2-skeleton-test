#include "ui.h"
#include "sdlwrap.h"

#include <algorithm>
#include <cassert>

namespace tG
{
	namespace UI
	{
		// Static variables
		//---
		Font symbols_font, default_font;
		SDL_Color default_text_color = {0,0,0},
		          default_marker_color = {255,166,77},
		          selected_marker_color = {Bone::colors[Bone::SELECTED].r,Bone::colors[Bone::SELECTED].g,Bone::colors[Bone::SELECTED].b},
		          default_marker_border_color = {77,77,77},
		          default_new_button_out_color = {179,179,179};
		ColorRGBA default_border_color = {0,0,0,255},
		          default_bg_color = {179,179,179,255},
		          mouse_over_bg_color = {125,161,179,255},
		          default_new_button_cross_color = {54,179,54,255},
		          default_shadow_color = {77,77,77,127};
		int default_border = 4;

		std::vector<I_GUIMouse*> all_GUIMouse;
		std::vector<I_GUIKeyboard*> all_GUIKeyboard;
		std::vector<I_Renderable*> all_renderables;

		const std::string SYMBOL_ARROW_DOWN = "3";
		const std::string SYMBOL_ARROW_UP   = "2";
		const std::string SYMBOL_ARROW_LEFT   = "0";
		const std::string SYMBOL_MARKER   = "?";
		const std::string SYMBOL_NEW   = "i";

		SDLWrap_Texture marker_texture,
		                marker_selected_texture,
		                marker_border_texture;
		int marker_symbol_width,
		    marker_symbol_top_blank_space;
		
		SDLWrap_Texture new_button_texture,
		                new_button_border_texture,
		                new_button_inshadow_texture;

		Rectangle AnimationTimeline::new_keyframe_button_AABB;


		// Static fuctions
		//---
		void init()
		{
			default_font = Font::getFont(Font("./fonts/Inconsolata/Inconsolata-Regular.ttf", 14));
			symbols_font = Font::getFont(Font("./fonts/ikoo/ikoo.ttf", 25));

			Font symbols_font_plus1 = Font::getFont(Font(symbols_font.getPath(), symbols_font.getSize()+4));
			Font symbols_font_minus1 = Font::getFont(Font(symbols_font.getPath(), symbols_font.getSize()-4));
			Font symbols_font_minus2 = Font::getFont(Font(symbols_font.getPath(), symbols_font.getSize()-8));

			// marker
			//---
			marker_texture = Font::renderTextOnTexture(SYMBOL_MARKER, symbols_font, default_marker_color);
			int ret = SDL_SetTextureBlendMode(marker_texture.texture, SDL_BLENDMODE_BLEND);
			int minx, maxx, miny, maxy, advance;
			ret =  TTF_GlyphMetrics(symbols_font.getFont(), SYMBOL_MARKER.c_str()[0], &minx, &maxx, &miny, &maxy, &advance);
			std::cout<<"minx = "<<minx<<" maxx = "<<maxx<<" miny = "<<miny<<" maxy = "<<maxy<<" advance = "<<advance<<" ["<<SYMBOL_MARKER<<"]"<<std::endl;
			marker_symbol_width = maxx-minx;
			marker_symbol_top_blank_space = symbols_font.getMaxHeight()-maxy;
			marker_texture.size.x = minx;
			marker_texture.size.y = marker_symbol_top_blank_space;
			marker_texture.size.w = maxx-minx;
			marker_texture.size.h = maxy-miny;
			marker_selected_texture = Font::renderTextOnTexture(SYMBOL_MARKER, symbols_font, selected_marker_color);
			ret = SDL_SetTextureBlendMode(marker_selected_texture.texture, SDL_BLENDMODE_BLEND);
			marker_selected_texture.size = marker_texture.size;

			// marker border
			//---
			marker_border_texture = Font::renderTextOnTexture(SYMBOL_MARKER, symbols_font_plus1, default_marker_border_color);
			ret = SDL_SetTextureBlendMode(marker_border_texture.texture, SDL_BLENDMODE_BLEND);
			ret =  TTF_GlyphMetrics(symbols_font_plus1.getFont(), SYMBOL_MARKER.c_str()[0], &minx, &maxx, &miny, &maxy, &advance);
			std::cout<<"minx = "<<minx<<" maxx = "<<maxx<<" miny = "<<miny<<" maxy = "<<maxy<<" advance = "<<advance<<" ["<<SYMBOL_MARKER<<"]"
			         <<" maxh of font="<<symbols_font_plus1.getMaxHeight()
			         <<" ascent of font="<<TTF_FontAscent(symbols_font_plus1.getFont())<<std::endl;
			marker_border_texture.size.x = minx;
			marker_border_texture.size.y = symbols_font_plus1.getMaxHeight()-maxy;
			marker_border_texture.size.w = maxx-minx;
			marker_border_texture.size.h = maxy-miny;

			// new button
			//---
			new_button_texture = Font::renderTextOnTexture(SYMBOL_NEW, symbols_font_minus2, default_new_button_out_color);
			ret = SDL_SetTextureBlendMode(new_button_texture.texture, SDL_BLENDMODE_BLEND);
			ret =  TTF_GlyphMetrics(symbols_font_minus2.getFont(), SYMBOL_NEW.c_str()[0], &minx, &maxx, &miny, &maxy, &advance);
			std::cout<<"minx = "<<minx<<" maxx = "<<maxx<<" miny = "<<miny<<" maxy = "<<maxy<<" advance = "<<advance<<" ["<<SYMBOL_NEW<<"]"<<std::endl;
			std::cout<<" new_button_texture.size.x="<<new_button_texture.size.x<<std::endl
			         <<" new_button_texture.size.y="<<new_button_texture.size.y<<std::endl
			         <<" new_button_texture.size.w="<<new_button_texture.size.w<<std::endl
			         <<" new_button_texture.size.h="<<new_button_texture.size.h<<std::endl
			         <<" maxh of font="<<symbols_font_minus2.getMaxHeight()<<std::endl
			         <<" ascent of font="<<TTF_FontAscent(symbols_font_minus2.getFont())<<std::endl;
			new_button_texture.size.x = minx;
			new_button_texture.size.y = TTF_FontAscent(symbols_font_minus2.getFont())-maxy;
			new_button_texture.size.w = maxx-minx;
			new_button_texture.size.h = maxy-miny;
			std::cout<<" new_button_texture.size.x="<<new_button_texture.size.x<<std::endl
			         <<" new_button_texture.size.y="<<new_button_texture.size.y<<std::endl
			         <<" new_button_texture.size.w="<<new_button_texture.size.w<<std::endl
			         <<" new_button_texture.size.h="<<new_button_texture.size.h<<std::endl
			         <<" maxh of font="<<symbols_font_minus2.getMaxHeight()<<std::endl;

			// new button border
			//---
			new_button_border_texture = Font::renderTextOnTexture(SYMBOL_NEW, symbols_font_minus1, default_border_color);
			ret = SDL_SetTextureBlendMode(new_button_border_texture.texture, SDL_BLENDMODE_BLEND);
			ret =  TTF_GlyphMetrics(symbols_font_minus1.getFont(), SYMBOL_NEW.c_str()[0], &minx, &maxx, &miny, &maxy, &advance);
			std::cout<<"minx = "<<minx<<" maxx = "<<maxx<<" miny = "<<miny<<" maxy = "<<maxy<<" advance = "<<advance<<" ["<<SYMBOL_NEW<<"]"<<std::endl;
			new_button_border_texture.size.x = minx;
			new_button_border_texture.size.y = TTF_FontAscent(symbols_font_minus1.getFont())-maxy;
			new_button_border_texture.size.w = maxx-minx;
			new_button_border_texture.size.h = maxy-miny;

			// new button in shadow
			//---
			new_button_inshadow_texture = Font::renderTextOnTexture(SYMBOL_NEW, symbols_font_minus1, default_shadow_color);
			ret = SDL_SetTextureBlendMode(new_button_inshadow_texture.texture, SDL_BLENDMODE_BLEND);
			ret =  TTF_GlyphMetrics(symbols_font_minus1.getFont(), SYMBOL_NEW.c_str()[0], &minx, &maxx, &miny, &maxy, &advance);
			std::cout<<"minx = "<<minx<<" maxx = "<<maxx<<" miny = "<<miny<<" maxy = "<<maxy<<" advance = "<<advance<<" ["<<SYMBOL_NEW<<"]"<<std::endl;
			new_button_inshadow_texture.size.x = minx;
			new_button_inshadow_texture.size.y = TTF_FontAscent(symbols_font_minus1.getFont())-maxy;
			new_button_inshadow_texture.size.w = maxx-minx;
			new_button_inshadow_texture.size.h = maxy-miny;

			AnimationTimeline::init();
		}
		void render()
		{
			for (I_Renderable* r : all_renderables)
			{
				r->render();
				// TODO visibility and active/unactive
			}
		}
		void registerGUIMouse(I_GUIMouse* component)
		{
			std::cout<<"registering GUIMouse component"<<std::endl;
			all_GUIMouse.push_back(component);
		}
		void registerGUIKeyboard(I_GUIKeyboard* component)
		{
			std::cout<<"registering GUIKeyboard component"<<std::endl;
			all_GUIKeyboard.push_back(component);
		}
		void registerRenderable(I_Renderable* component)
		{
			std::cout<<"registering Renderable component"<<std::endl;
			all_renderables.push_back(component);
		}
		bool mouseMovedInGUI(const TGMath::i::Vec2& m_pos, const TGMath::i::Vec2& m_mov) /// returns true if the movement was processed by the GUI, false if it has to be processed by the game
		{
			bool res = false;
			for (I_GUIMouse* gm : all_GUIMouse)
			{
				res |= gm->mouseMovedInGUI(m_pos, m_mov);
			}
			return res;
		}
		bool mouseButtonInGUI(const TGMath::i::Vec2& m_pos, MouseButton which, bool up) /// returns true if the movement was processed by the GUI, false if it has to be processed by the game
		{
			bool res = false;
			for (I_GUIMouse* gm : all_GUIMouse)
			{
				res |= gm->mouseButtonInGUI(m_pos, which, up);
			}
			return res;
		}
		bool mouseWheelInGUI(const TGMath::i::Vec2& m_pos, bool up) /// returns true if the movement was processed by the GUI, false if it has to be processed by the game
		{
			bool res = false;
			for (I_GUIMouse* gm : all_GUIMouse)
			{
				res |= gm->mouseWheelInGUI(m_pos, up);
			}
			return res;
		}

		bool keyPressed(int SDL_key, const Uint8* keys)
		{
			bool res = false;
			for (I_GUIKeyboard* gk : all_GUIKeyboard)
			{
				res |= gk->keyPressed(SDL_key, keys);
			}
			return res;
		}
		bool keyReleased(int SDL_key, const Uint8* keys)
		{
			bool res = false;
			for (I_GUIKeyboard* gk : all_GUIKeyboard)
			{
				res |= gk->keyReleased(SDL_key, keys);
			}
			return res;
		}

		I_GUIMouse::I_GUIMouse()
		{
			// register
			//---
			tG::UI::registerGUIMouse(this);
		}
		I_GUIKeyboard::I_GUIKeyboard()
		{
			// register
			//---
			tG::UI::registerGUIKeyboard(this);
		}
		I_Renderable::I_Renderable()
		{
			// register
			//---
			tG::UI::registerRenderable(this);
		}

		bool Rectangle::isPointInside(const TGMath::i::Vec2& p) const
		{	
			return pos_tl.x <= p.x && p.x <= pos_tl.x+width &&
			       pos_tl.y - height <= p.y && p.y <= pos_tl.y;
		}

		//////////////////////////////////////////////////////////////////////////

		// ConstDropDown
		//---

		// protected
		//---
		int ConstDropDown::whichLine(int y_carth) const
		{
			int y = pos.y - line_h - y_carth;
			if (y<=0)
				y-=line_h;
			return std::min(y / line_h, (int)text_textures.size()-1);
		}
		void ConstDropDown::updateSize()
		{
			if (hasFlag(FIXED_SIZE))
				return;

			SDLWrap_Texture texture = text_textures[selected];
			SDL_Rect rt = texture.size;
			int ww, wh;
			SDL_GetWindowSize(  Environment::window, // SDL_Window* window: the window to query
			                    &ww,    // int* w: gets the width of the window
			                    &wh);   // int* h: gets the height of the window


			if (isOpen())
			{
				for (const SDLWrap_Texture& t : text_textures)
				{
					if (t.size.w > rt.w)
					{
						rt.w = t.size.w;
					}
					if (t.size.h > rt.h)
					{
						rt.h = t.size.h;
					}
				}
			}

			if (!hasFlag(FIXED_WIDTH))
			{
				width = rt.w+2*default_border;
				if (max_width>0)
					width = std::min(width, max_width);
				width = std::max(width, min_width);
			}
			if (!hasFlag(FIXED_HEIGHT))
			{
				height = rt.h+2*default_border;
				if (max_height>0)
					height = std::min(height, max_height);
				height = std::max(height, min_height);
			}
		}
		void ConstDropDown::select(int id)
		{
			int old = selected;
			selected = id;
			std::cout<<"selected "<<id<<std::endl;
			if (onSelectCallback) onSelectCallback(selected, old, text_strings);
		}

		// public
		//---
		ConstDropDown::ConstDropDown(const std::set<std::string>& strings) : 
		                            flags(0x0),
		                            text_textures(std::vector<SDLWrap_Texture>(strings.size())),
		                            text_strings(std::vector<const std::string*>(strings.size())),
		                            is_open(false),
		                            strings(strings)
		{
			int i=0;
			for (const std::string& s : strings)
			{
				std::cout<<"CDD has string "<<s<<std::endl;
				std::cout<<"text_strings.size() = "<<text_strings.size()<<std::endl;
				text_strings[i] = &s;
				text_textures[i] = Font::renderTextOnTexture(s, default_font, default_text_color);
				i++;
			}
			selected = 0;
			std::cout<<"selected string is "<<*text_strings[selected]<<std::endl;
		}

		void ConstDropDown::setFlag(uint8_t flag)
		{
			flags |= flag;
		}
		void ConstDropDown::unsetFlag(uint8_t flag)
		{
			flags &= ~flag;
		}
		bool ConstDropDown::hasFlag(uint8_t flag) const
		{
			return (flags & flag) == flag;
		}
		void ConstDropDown::setWidth(uint width)
		{
			this->width = width;
		}
		void ConstDropDown::setHeight(uint height)
		{
			this->height = height;
		}
		uint ConstDropDown::getWidth() const
		{
			return width;
		}
		uint ConstDropDown::getHeight() const
		{
			return height;
		}
		void ConstDropDown::setMinWidth(uint min_width)
		{
			this->min_width = min_width;
		}
		void ConstDropDown::setMinHeight(uint min_height)
		{
			this->min_height = min_height;
		}
		uint ConstDropDown::getMinWidth() const
		{
			return min_width;
		}
		uint ConstDropDown::getMinHeight() const
		{
			return min_height;
		}
		void ConstDropDown::setMaxWidth(uint max_width)
		{
			this->max_width = max_width;
		}
		void ConstDropDown::setMaxHeight(uint max_height)
		{
			this->max_height = max_height;
		}
		uint ConstDropDown::getMaxWidth() const
		{
			return max_width;
		}
		uint ConstDropDown::getMaxHeight() const
		{
			return max_height;
		}

		bool ConstDropDown::isOpen() const
		{
			return is_open;
		}
		bool ConstDropDown::isClosed() const
		{
			return !is_open;
		}

		void ConstDropDown::open()
		{
			is_open = true;
		}
		void ConstDropDown::close()
		{
			is_open = false;
		}

		void ConstDropDown::render()
		{
			updateSize();

			if (isClosed())
			{
				// Render closed
				//---
				SDLWrap_Texture texture = text_textures[selected];
				SDL_Rect rt = texture.size,
				         r;

				rt.w = std::min(rt.w,width-default_border*2);
				rt.h = std::min(rt.h,height-default_border*2);

				rt.x = pos.x+((int)height-rt.h)/2;
				rt.y = pos.y-((int)height-rt.h)/2;

				texture.size.w = std::min(texture.size.w,rt.w);

				if (rt.h < texture.size.h)
				{
					texture.size.y += (texture.size.h-height)/2 + default_border;
					texture.size.h = rt.h;
				}
				int ww, wh;
				SDL_GetWindowSize(  Environment::window, // SDL_Window* window: the window to query
				                    &ww,    // int* w: gets the width of the window
				                    &wh);   // int* h: gets the height of the window

				rt.x = rt.x;
				rt.y = wh - rt.y;

				r.w = width;
				r.h = height;
				r.x = pos.x;
				r.y = wh - pos.y;
				
				renderRectPlain(r, default_bg_color);
				renderTexturedRect(texture,rt);
				renderRectBorder(r, default_border_color);

				// update rectangle bounding box
				//---
				SDL_Rect rbb = r;
				rbb_w = rbb.w += 2*default_border+14; // < hey, look, a magic number!
				rbb_h = rbb.h;
				// renderRectBorder(rbb, {111,0,0,255});

				// render arrow
				//---
				SDLWrap_Texture sym_texture = Font::renderTextOnTexture(SYMBOL_ARROW_DOWN, symbols_font);
				SDL_Rect sym_r = sym_texture.size;
				sym_r.x = pos.x+width+default_border;
				sym_r.y = wh - (pos.y-((int)height-sym_r.h)/2);
				renderTexturedRect(sym_texture, sym_r);
			} else
			{
				// Render Open
				//---
				SDL_Rect rt,
				         r,
				         r_all;

				int ww, wh;
				SDL_GetWindowSize(  Environment::window, // SDL_Window* window: the window to query
				                    &ww,    // int* w: gets the width of the window
				                    &wh);   // int* h: gets the height of the window

				r.w = width;
				r.h = height;
				r.x = pos.x;
				r.y = wh - pos.y;

				// update rectangle bounding box
				//---
				SDL_Rect rbb = r;
				line_h = rbb.h;
				rbb.y = wh - pos.y;
				rbb_w = rbb.w += 2*default_border+14; // < hey, look, a magic number!
				rbb_h = rbb.h *= (text_textures.size()+1);
				rbb.h = height;
				// renderRectBorder(rbb, {111,0,0,255});
				
				// r.y += 1;
				// r.x += 1;
				// r.w -= 1;
				// r.h -= 1;
				// renderRectPlain(r, default_bg_color);
				// r.y -= 1;
				// r.x -= 1;
				// r.w += 1;
				// r.h += 1;

				// render all bg
				//---
				r_all.x = r.x;
				r_all.y = r.y;
				r_all.w = width;
				r_all.h = height*(text_strings.size()+1);
				renderRectPlain(r_all, default_bg_color);

				r.y += height;

				int id=0;
				for (SDLWrap_Texture texture : text_textures)
				{
					rt = texture.size;

					rt.w = std::min(rt.w,width-default_border*2);
					rt.h = std::min(rt.h,height-default_border*2);

					rt.x = r.x+((int)height-rt.h)/2;
					rt.y = wh-r.y-((int)height-rt.h)/2;

					texture.size.w = std::min(texture.size.w,rt.w);

					if (rt.h < texture.size.h)
					{
						texture.size.y += (texture.size.h-height)/2 + default_border;
						texture.size.h = rt.h;
					}

					rt.x = rt.x;
					rt.y = wh - rt.y;
					
					if (id==mouse_over_line)
						renderRectPlain(r, mouse_over_bg_color);
					// renderRectPlain(r, default_bg_color);
					renderTexturedRect(texture,rt);
					if (id==selected)
					{
						// renderRectBorder(r, {111,173,255,255});
						r.w += 1;
						renderRectBorder(r, {0,0,0,255});
						r.w -= 1;

						// render arrow left
						//---
						SDLWrap_Texture sym_texture = Font::renderTextOnTexture(SYMBOL_ARROW_LEFT, symbols_font);
						SDL_Rect sym_r = sym_texture.size;
						sym_r.x = pos.x+width+default_border;
						sym_r.y = r.y + ((int)height-sym_r.h)/2;
						renderTexturedRect(sym_texture, sym_r);
					}

					r.y += height;
					id++;
				}

				// render border
				//---
				renderRectBorder(r_all, default_border_color);


				// render arrow up
				//---
				SDLWrap_Texture sym_texture = Font::renderTextOnTexture(SYMBOL_ARROW_UP, symbols_font);
				SDL_Rect sym_r = sym_texture.size;
				sym_r.x = pos.x+width+default_border;
				sym_r.y = wh - (pos.y-((int)height-sym_r.h)/2);
				renderTexturedRect(sym_texture, sym_r);
			}
		}

		// I_GUIMouse
		//---
		bool ConstDropDown::isMouseOnMe(const TGMath::i::Vec2& m_pos)
		{
			return pos.x <= m_pos.x && m_pos.x <= pos.x+rbb_w &&
			       pos.y-rbb_h <= m_pos.y && m_pos.y <= pos.y;
		}
		bool ConstDropDown::mouseMovedInGUI(const TGMath::i::Vec2& m_pos, const TGMath::i::Vec2& m_mov) /// returns true if the movement was processed by the GUI, false if it has to be processed by the game
		{
			if (!isMouseOnMe(m_pos))
			{
				if (isOpen())
					mouse_over_line = -1;

				return false;
			}
			if (isOpen() && m_pos.x<=pos.x+width)
				mouse_over_line = whichLine(m_pos.y);
			else
				mouse_over_line = -1;
			return false; // 'cause for now we do nothing with movement
		}
		bool ConstDropDown::mouseButtonInGUI(const TGMath::i::Vec2& m_pos, MouseButton which, bool up) /// returns true if the movement was processed by the GUI, false if it has to be processed by the game
		{
			if (!isMouseOnMe(m_pos))
			{
				return false;
			}

			if (which != LEFT_MB)
			{
				return true;
			}

			if (up)
			{
				return true;
			}

			if (isClosed())
			{
				open();
				return true;
			}
			else
			{
				if (m_pos.y > pos.y-height)
				{
					// mouse is above
					close();
					return true;
				}
				if (m_pos.x>pos.x+width)
				{
					return false;
				}

				mouse_over_line = whichLine(m_pos.y);
				select(mouse_over_line);
				close();
				return true;

				// const int N=text_textures.size();
				// for (int id=0; id!=N; id++)
				// {
				// 	if (m_pos.y >= pos.y-(id+2)*height)
				// 	{
				// 		select(id);
				// 		close();
				// 		return true;
				// 	}
				// 	std::cout<<"mouse is below "<<pos.y-(id+2)*height<<std::endl;
				// }
				// assert(false);
			}
		}
		bool ConstDropDown::mouseWheelInGUI(const TGMath::i::Vec2& m_pos, bool up) /// returns true if the movement was processed by the GUI, false if it has to be processed by the game
		{
			if (!isMouseOnMe(m_pos))
				return false;
			return false; // 'cause for now we do nothing with wheel TODO scrolling for too long lists
		}

		//////////////////////////////////////////////////////////////////////////

		// AnimationTimeline
		//---
		void AnimationTimeline::init()
		{
			new_keyframe_button_AABB.pos_tl.x = new_button_border_texture.size.x;
			new_keyframe_button_AABB.pos_tl.y = new_button_border_texture.size.y;
			new_keyframe_button_AABB.width = new_button_border_texture.size.w;
			new_keyframe_button_AABB.height = new_button_border_texture.size.h;
		}
		const ColorRGBA AnimationTimeline::notches_color = {128,128,128,255};
		AnimationTimeline::AnimationTimeline() : skeleton(nullptr),
		                                         active_animation(nullptr),
		                                         mouse_over_line(-1),
		                                         selected_bone(-1),
		                                         selected_keyframe(nullptr),
		                                         animating(false),
		                                         pxps(20)
		{}
		AnimationTimeline::AnimationTimeline(Skeleton* skeleton) : skeleton(skeleton),
		                                         active_animation(nullptr)
		{
			setSkeleton(skeleton);
		}

		// protected
		// methods
		void AnimationTimeline::selectKeyframe(std::shared_ptr<Bone::Keyframe> keyframe)
		{
			selected_keyframe = keyframe;

			if (selected_keyframe!=nullptr)
			{
				skeleton->startAnimation(*active_animation, 0);
				skeleton->animateAtRelativeTime(selected_keyframe->time);
				animating = false;
			}
			if (onSelectKeyframeCallback)
				onSelectKeyframeCallback(keyframe);
		}

		bool AnimationTimeline::setAnimation(const std::string& animation_name)
		{
			if (!skeleton->hasAnimation(animation_name))
			{
				return false;
			}
			active_animation = &animation_name;
			return true;
		}
		void AnimationTimeline::setSkeleton(Skeleton* skeleton)
		{
			this->skeleton = skeleton;

			auto bones = skeleton->getBones();
			names_textures.resize(bones.size());

			int i=0, max_w=0;
			for (Bone* b : bones)
			{
				std::cout<<"skeleton has bone #"<<i<<" "<<b->name<<std::endl;
				std::cout<<"names_textures.size() "<<names_textures.size()<<std::endl;
				names_textures[i] = Font::renderTextOnTexture(b->name, default_font, default_text_color);
				if (max_w<names_textures[i].size.w)
				{
					max_w = names_textures[i].size.w;
				}
				i++;
			}
			names_max_w = max_w;
		}
		Skeleton* AnimationTimeline::getSkeleton()
		{
			return skeleton;
		}
		const std::string& AnimationTimeline::getActiveAnimation() const
		{
			return *active_animation;
		}
		bool AnimationTimeline::isAnimating() const
		{
			return animating;
		}
		void AnimationTimeline::selectBone(Bone* bone)
		{
			if (bone==nullptr)
			{
				selectBone(-1);
				return;
			}

			int bid = 0;
			for (const Bone* b : skeleton->bones)
			{
				if (bone == b)
				{
					selectBone(bid);
					return;
				}
				bid++;
			}
		}
		void AnimationTimeline::selectBone(int bid)
		{
			if (selected_bone != bid)
			{
				selectKeyframe(nullptr);
			}
			selected_bone = bid;
			if (bid == -1)
			{
				Bone::selectBone(nullptr);	
				return;
			}
			Bone::selectBone(skeleton->bones[selected_bone]);
		}

		void AnimationTimeline::update(Time game_time)
		{
			this->game_time = game_time;

			if (animating)
			{
				skeleton->animate(game_time);
			}
		}
		
		void AnimationTimeline::render()
		{
			if (skeleton==nullptr)
			{
				return;
			}

			int ww,wh;
			SDL_GetWindowSize(  Environment::window, // SDL_Window* window: the window to query
			                    &ww,    // int* w: gets the width of the window
			                    &wh);   // int* h: gets the height of the window

			const std::deque<Bone*>& bones = skeleton->getBones();
			const int bc             = names_textures.size(),
			          line_h         = default_font.getMaxHeight() + 2*default_border,
			          margin_bottom  = line_h,
			          margin_left    = line_h,
			          margin_right   = line_h,
			          top            = (margin_bottom + line_h*bc),
			          end_names      = margin_left + names_max_w + 2*default_border,
			          timeline_width = ww - end_names - margin_right,
			          notches_h      = line_h * 4 / 10,
			          notches_5_h    = line_h * 6 / 10;

         // set bounding box
			AABB.pos_tl.x = margin_left;
			AABB.pos_tl.y = top;
			AABB.width = ww - margin_left - margin_right;
			AABB.height = top - margin_bottom;
			this->line_h = line_h;

			int x = margin_left,
			    y = wh - top;

			// render background
			renderRectPlain({margin_left, y, AABB.width, AABB.height}, default_bg_color);
			if (mouse_over_line>=0)
			{
				renderRectPlain({margin_left, mouse_over_line*line_h+y, AABB.width, line_h}, mouse_over_bg_color);
			}

			// render bones names
			//---
			SDL_Rect r;
			for (const SDLWrap_Texture& t : names_textures)
			{
				r = t.size;
				r.x = x+default_border;
				r.y = y+default_border;
				renderTexturedRect(t, r);

				// render time steps
				//---
				setRenderColor(Environment::renderer, AnimationTimeline::notches_color);
				for (int i=0, nx=end_names; nx<ww-margin_right; nx+=pxps, i++)
				{
					if (i==5) i=0;
					int h = i==0 ? notches_5_h : notches_h;
					SDL_RenderDrawLine(Environment::renderer, nx,y, nx,y+h);
				}

				// line separator
				setRenderColor(Environment::renderer, {0,0,0,255});
				SDL_RenderDrawLine(Environment::renderer, x,y, x+ww-margin_left-margin_right,y);

				if (!animating)
				{
					// new keyframe button
					//---
					SDL_Rect nb_r = new_button_texture.size;
					int the_x = margin_left - default_border - nb_r.w/2;
					nb_r.x = the_x - nb_r.w/2;
					nb_r.y = y + (line_h - nb_r.w)/2;
					SDL_Rect nb_b_r = new_button_border_texture.size;
					nb_b_r.w = nb_r.w + 2;
					nb_b_r.h = nb_r.h + 2;
					nb_b_r.y = nb_r.y - 1;
					nb_b_r.x = nb_r.x - 1;
					SDL_Rect nb_b_in_r = new_button_border_texture.size;
					nb_b_in_r.w = nb_r.w - 2;
					nb_b_in_r.h = nb_r.h - 2;
					nb_b_in_r.y = nb_r.y + 1;
					nb_b_in_r.x = nb_r.x + 1;

					SDL_Rect wb = nb_r;
					wb.x += nb_r.w*2/10;
					wb.y += nb_r.h*2/10;
					wb.w = nb_r.w*8/10;
					wb.h = nb_r.h*8/10;

					// renderRectBorder(nb_b_r, {0,0,255,0});
					renderTexturedRect(new_button_border_texture, nb_b_r, {255,255,255,255});
					renderRectPlain(wb, default_new_button_cross_color);
					renderTexturedRect(new_button_inshadow_texture, nb_b_in_r, {255,255,255,127});
					renderTexturedRect(new_button_texture, nb_r);
				}


				y += line_h;
			}
			setRenderColor(Environment::renderer, {0,0,0,255});
			// bottom border
			SDL_RenderDrawLine(Environment::renderer, x,y, x+ww-margin_left-margin_right,y);
			// left and right borders
			SDL_RenderDrawLine(Environment::renderer, x,wh-top, x,wh-margin_bottom);
			SDL_RenderDrawLine(Environment::renderer,
				                ww-margin_right, wh-top,
				                ww-margin_right, wh-margin_bottom);
			// names separator from timeline
			SDL_RenderDrawLine(Environment::renderer, end_names,wh-top, end_names,wh-margin_bottom);

			// markers
			//---
		   y = wh - top;
		   int line_id = 0;
		   lines.resize(bones.size());
			for (const Bone* b : bones)
			{
				const Bone::Animation* a;
				try
				{
					a = &b->animations.at(*active_animation);
				} catch (std::out_of_range e) // ???
				{
					continue;
				}

				std::vector<Marker>& markers = lines[line_id];
				
				int last_x, ol_count=0, the_x;
				int marker_id = 0;
				markers.resize(a->keyframes.size());
				for (std::shared_ptr<Bone::Keyframe> k : a->keyframes)
				{
					the_x = end_names + k->time*pxps;
					SDL_Rect sym_r = marker_texture.size;
					sym_r.x = the_x - sym_r.w/2;
					sym_r.y = y + 3;
					// renderRectBorder(sym_r, {0,0,255,0});
					setRenderColor(Environment::renderer, {255,0,0,255});
					SDL_RenderDrawLine(Environment::renderer, the_x,y, the_x,y+line_h);


					SDL_Rect sym_b_r = marker_border_texture.size;
					sym_b_r.w = sym_r.w+2;
					sym_b_r.h = sym_r.h+2;
					sym_b_r.x = the_x - sym_b_r.w/2;
					sym_b_r.y = y + 3 - (sym_b_r.h-sym_r.h)/2;

					renderTexturedRect(marker_border_texture, sym_b_r, {255,255,255,127});
					if (k==selected_keyframe)
						renderTexturedRect(marker_selected_texture, sym_r);
					else
						renderTexturedRect(marker_texture, sym_r);

					if (sym_r.x==last_x)
					{
						ol_count++;
					} else
					if (ol_count!=0)
					{
						Font::renderText(std::to_string(ol_count+1), default_font, last_x, wh-y-3);
						ol_count = 0;
					}
					last_x = sym_r.x;

					markers[marker_id].AABB = {TGMath::i::Vec2(sym_b_r.x,wh-sym_b_r.y),sym_b_r.w,sym_b_r.h};
					markers[marker_id].keyframe = k;
					marker_id++;
				}

				if (ol_count!=0)
				{
					Font::renderText(std::to_string(ol_count+1), default_font, last_x, wh-y-3);
				}

				// time notch
				//---
				setRenderColor(Environment::renderer, {255,0,0,255});
				the_x = end_names + b->getRelativeTime()*pxps;
				SDL_RenderDrawLine(Environment::renderer, the_x,y, the_x,y+line_h);

				y += line_h;
				line_id++;
			}

		}
		bool AnimationTimeline::isMouseOnMe(const TGMath::i::Vec2& m_pos)
		{
			if (!animating)
			{
				Rectangle aabb = AABB;
				aabb.pos_tl.x -= default_border + new_keyframe_button_AABB.width;
				aabb.width += default_border + new_keyframe_button_AABB.width;
				return aabb.isPointInside(m_pos);
			}
      	return AABB.isPointInside(m_pos);
		}
		bool AnimationTimeline::mouseMovedInGUI(const TGMath::i::Vec2& m_pos, const TGMath::i::Vec2& m_mov) /// returns true if the movement was processed by the GUI, false if it has to be processed by the game
		{
			if (!isMouseOnMe(m_pos))
			{
				mouse_over_line = -1;
				return false;
			}

			mouse_over_line = whichLine(m_pos.y);
			
			return true;
		}
		bool AnimationTimeline::mouseButtonInGUI(const TGMath::i::Vec2& m_pos, MouseButton which, bool up) /// returns true if the movement was processed by the GUI, false if it has to be processed by the game
		{
			if (!isMouseOnMe(m_pos))
				return false;

			// click on keyframe marker
			if (which==LEFT_MB && !up)
			{
				Rectangle b_AABB = new_keyframe_button_AABB;
				b_AABB.pos_tl.x = AABB.pos_tl.x - default_border - b_AABB.width;
				int lid = 0;
				for (auto l : lines)
				{
					if (!animating)
					{
						b_AABB.pos_tl.y = AABB.pos_tl.y - lid*line_h;
						if (b_AABB.isPointInside(m_pos))
						{
							skeleton->bones[lid]->addKeyframeNow();
							// TODO select keyframe
							return true;
						}
					}

					int id = 0;
					for (Marker& m : l)
					{
						if (m.AABB.isPointInside(m_pos))
						{
							std::cout<<"selected keyframe #"<<id<<" of line #"<<lid<<std::endl;
							selectBone(lid);
							selectKeyframe(m.keyframe);
						}
						id++;
					}
					lid++;
				}
			}
			return true;
		}
		bool AnimationTimeline::mouseWheelInGUI(const TGMath::i::Vec2& m_pos, bool up) /// returns true if the movement was processed by the GUI, false if it has to be processed by the game
		{
			return false;
		}

		// I_GUIKeyboard
		//---
		bool AnimationTimeline::keyPressed(int SDL_key, const Uint8* keys)
		{
			switch (SDL_key)
			{
				case SDLK_SPACE:
				{
	 				if (!animating)
	 				{
						skeleton->startAnimation(*active_animation, game_time);
	 					animating = true;
	 					selectKeyframe(nullptr);
					} else
					{
						animating = false;
						skeleton->stopAnimation();
					}
					return true;
				}
			}
			return false;
		}
		bool AnimationTimeline::keyReleased(int SDL_key, const Uint8* keys)
		{
			return false;
		}

		// protected
		int AnimationTimeline::whichLine(int y_carth) const
		{
			int y = AABB.pos_tl.y - y_carth;
			return std::min(y / line_h, static_cast<int>(skeleton->bones.size())-1);
		}

	} // UI namespace
}// tG namespace