#pragma once


#include <SDL2/SDL.h>
#include <SDL2/SDL_video.h>
#include <SDL_ttf.h>

#include "sdlwrap.h"

#include <string>
#include <map>

class Font
{
private:
	TTF_Font *font;
	std::string path;
	int ptsize;
public:
	Font();
	Font(const Font& f);
	Font(Font&& f);
	Font(std::string path, int ptsize);

	Font& operator =(Font&& f);
	Font& operator =(const Font& f);

	bool isOK() const;

	int getSize() const;
	const std::string& getPath() const;
    const TTF_Font* getFont() const;
    TTF_Font* getFont();
    int getMaxHeight() const;

	static std::map<std::string, std::map<int, Font>> loaded_fonts;
	static void renderText(const std::string& text, const Font& font, int x, int y, const SDL_Color& color={0,0,0}, Uint8 alpha=255);
	static SDLWrap_Texture renderTextOnTexture(const std::string& text, const Font& font, SDL_Color color={0,0,0}, int outline = -1);
	static Font& getFont(Font& font);
	static Font&& getFont(Font&& font);
};