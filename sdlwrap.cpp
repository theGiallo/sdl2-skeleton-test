#include "sdlwrap.h"

ColorRGBA::operator SDL_Color()
{
	return {r,b,g};
}

void getRenderColor(SDL_Renderer* renderer, ColorRGBA& color)
{
	int ret = SDL_GetRenderDrawColor(renderer,
	                                 &color.r,
	                                 &color.g,
	                                 &color.b,
	                                 &color.a);
	DCHECKSDLERROR(ret);
}
void setRenderColor(SDL_Renderer* renderer, const ColorRGBA& color)
{
	int ret = SDL_SetRenderDrawColor(renderer,
	                                 color.r,
	                                 color.g,
	                                 color.b,
	                                 color.a);
	DCHECKSDLERROR(ret);
}
bool renderTexturedRect(const SDLWrap_Texture& texture, const SDL_Rect& rect, const ColorRGBA& color)
{
	auto t = texture;
	int ret;
	ColorRGBA orig_c, new_c;
	ret = SDL_GetTextureAlphaMod(t.texture, &orig_c.a);
	new_c.a = ((int)color.a * (int)orig_c.a) / 255;
	ret = SDL_SetTextureAlphaMod(t.texture, new_c.a);
	ret = SDL_GetTextureColorMod(t.texture, &orig_c.r, &orig_c.g, &orig_c.b);
	new_c.r = ((int)color.r * (int)orig_c.r) / 255;
	new_c.g = ((int)color.g * (int)orig_c.g) / 255;
	new_c.b = ((int)color.b * (int)orig_c.b) / 255;
	ret = SDL_SetTextureColorMod(t.texture, new_c.r, new_c.g, new_c.b);

	if (ret != 0)
	{
		const char *error = SDL_GetError();
		if (*error != '\0')
		{
			SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Some problem with Texture Mod. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
			SDL_ClearError();
			return false;
		}
	}
	ret =
		SDL_RenderCopy( Environment::renderer,	// SDL_Renderer* renderer: the renderer to affect
						t.texture,	// SDL_Texture* texture: the source texture
						&t.size,   // const SDL_Rect* srcrect: the source SDL_Rect structure or NULL for the entire texture  
						&rect);  // const SDL_Rect* dstrect: the destination SDL_Rect structure or NULL for the entire rendering target. The texture will be stretched to fill the given rectangle.
		if (ret != 0)
		{
			const char *error = SDL_GetError();
			if (*error != '\0')
			{
				SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not rendercopy. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
				SDL_ClearError();
				return false;
			}
		}

	ret = SDL_SetTextureColorMod(t.texture, orig_c.r, orig_c.g, orig_c.b);
	ret = SDL_SetTextureAlphaMod(t.texture, orig_c.a);
	if (ret != 0)
	{
		const char *error = SDL_GetError();
		if (*error != '\0')
		{
			SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Some problem with Texture Mod. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
			SDL_ClearError();
			return false;
		}
	}
	return true;
}
bool renderRectBorder(const SDL_Rect& rect, const ColorRGBA& color)
{
	ColorRGBA oldc;
	getRenderColor(Environment::renderer, oldc);
	setRenderColor(Environment::renderer, color);
	int ret = SDL_RenderDrawRect(Environment::renderer, &rect);
	if (ret != 0)
	{
		const char *error = SDL_GetError();
		if (*error != '\0')
		{
			SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not renderDrawRect. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
			SDL_ClearError();
		}
	}
	setRenderColor(Environment::renderer, oldc);
	return true;
}
bool renderRectPlain(const SDL_Rect& rect, const ColorRGBA& color)
{
	ColorRGBA oldc;
	getRenderColor(Environment::renderer, oldc);
	setRenderColor(Environment::renderer, color);
	int ret = SDL_RenderFillRect(Environment::renderer, &rect);
	if (ret != 0)
	{
		const char *error = SDL_GetError();
		if (*error != '\0')
		{
			SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not renderDrawRect. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
			SDL_ClearError();
		}
	}
	setRenderColor(Environment::renderer, oldc);
	return true;
}