#ifndef MATHCONSTS_H
  #define MATHCONSTS_H
namespace TGMath
{
    namespace f
    {
        const float H_PI  = 3.141592653589793238462643383279f / 2.0f;
        const float PI   = 3.141592653589793238462643383279f;
        const float _2PI = 3.141592653589793238462643383279f * 2.0f;
    };
    namespace d
    {
        const float H_PI  = 3.141592653589793238462643383279 / 2.0;
        const float PI   = 3.141592653589793238462643383279;
        const float _2PI = 3.141592653589793238462643383279 * 2.0;
    };
};// namespace
#endif