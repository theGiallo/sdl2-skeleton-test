#include "MathTools.h"

namespace TGMath
{
    namespace f
    {
        float (*sign)(float) = TGMath::sign<float>;
        float distFromLine(const Vec2& X, const Vec2& v0, const Vec2& v1, Vec2* out_P_on_seg, float* out_t)
        {
            Vec2 v0v1 = v1-v0;
            Vec2 v0X = X-v0;
            float t = (v0X*v0v1)/(v0v1*v0v1);
            if (out_P_on_seg!=nullptr)
                *out_P_on_seg = v0+v0v1*t;
            if (out_t!=nullptr)
                *out_t = t;
            return (X-*out_P_on_seg).getModule();
        }
        float distFromSegment(const Vec2& X, const Vec2& v0, const Vec2& v1, Vec2* out_P_on_seg, float* out_t)
        {
            float t;
            float d = distFromLine(X, v0, v1, out_P_on_seg, &t);
            if (t<0)
            {
                t=0;
                if (out_P_on_seg!=nullptr)
                {
                    *out_P_on_seg = v0;
                }
                d = (X-v0).getModule();
            }
            if (t>1)
            {
                t=1;
                if (out_P_on_seg!=nullptr)
                {
                    *out_P_on_seg = v1;
                }
                d = (X-v1).getModule();
            }
            if (out_t!=nullptr)
            {
                *out_t=t;
            }
            return d;
        }
    };
    namespace d
    {
        double (*sign)(double) = TGMath::sign<double>;
    };
};