#pragma once
// #ifndef MATHTOOLS_H
    // #define MATHTOOLS_H

#include "MathConsts.h"
#include "Vec2.h"
#include <cmath>

namespace TGMath
{
    template <typename T>
    inline T min(T a, T b) {return ((a)<(b)?(a):(b));};
    template <typename T>
    inline T max(T a, T b) {return ((a)<(b)?(a):(b));};
    template <typename T>
    inline T sign(T a) {return (((a) > 0) - ((a) < 0));};

    namespace f
    {
        inline float DEG2RAD(float a)
        {
            return a*PI/180.0f;
        };
        inline float RAD2DEG(float a)
        {
            return a*180.0f/PI;
        };

        extern float (*sign)(float);

        /**
         * Returns the distance between point X and the segment v0-v1.
         * If the ortogonal projection of X on the line v0v1 is outside from the
         * segment returns the distance from the nearest of v0 and v1.
         *
         * P = v0 + t*(v1-v0);
         *
         * If the out parameters are nullptr they are not assigned.
         **/
        float distFromSegment(const Vec2& X, const Vec2& v0, const Vec2& v1, Vec2* out_P_on_seg, float* out_t);

        /**
         * Returns the distance between point X and the line passing through v0 
         * and v1.
         *
         * P = v0 + t*(v1-v0);
         * 
         * If the out parameters are nullptr they are not assigned.
         **/
        float distFromLine(const Vec2& X, const Vec2& v0, const Vec2& v1, Vec2* out_P_on_seg, float* out_t);
    };
    namespace d
    {
        inline double DEG2RAD(double a)
        {
            return a*PI/180.0f;
        };
        inline double RAD2DEG(double a)
        {
            return a*180.0f/PI;
        };

        extern double (*sign)(double);
    };

    template <typename T>
    void linearInterpolationV(unsigned int length, T start[], T end[], T result[], float alpha)
    {
        float omalpha = 1 - alpha;
        for (int i=0; i!=length; i++)
        {
            result[i] = static_cast<float>(start[i])*alpha + static_cast<float>(end[i])*omalpha;
        }
    }
    // if you have a known at compile time length this is better
    template<typename T, unsigned int length>
    inline void linearInterpolationV(T start[], T end[], T result[], float alpha)
    {
        float omalpha = 1 - alpha;
        for (int i=0; i!=length; i++) // hopefully this loop will be unrolled
        {
            result[i] = static_cast<float>(start[i])*alpha + static_cast<float>(end[i])*omalpha;
        }
    }
    // use this with integers numbers
    template<typename T, unsigned int length>
    inline void linearInterpolationRoundV(T start[], T end[], T result[], float alpha)
    {
        float omalpha = 1 - alpha;
        for (int i=0; i!=length; i++) // hopefully this loop will be unrolled
        {
            result[i] = std::round(static_cast<float>(start[i])*alpha + static_cast<float>(end[i])*omalpha);
        }
    }
};
// #endif // MATHTOOLS_H