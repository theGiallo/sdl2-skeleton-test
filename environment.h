#pragma once

#include <SDL2/SDL.h>

namespace Environment
{
	extern SDL_Window* window;
	extern SDL_Renderer* renderer;
};