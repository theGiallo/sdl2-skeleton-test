################################################################################
# This is a very basic makefile. Later then we will improve it.
################################################################################

LIB_SDL2=-L"./libraries/SDL2/linux/lib64" -Wl,-rpath=./libraries/SDL2/linux/lib64 -lSDL2-2.0
I_SDL2=-I"./libraries/SDL2/linux/include/"
LIB_SDL2_TTF=-L"./libraries/SDL2_ttf/linux/lib64" -Wl,-rpath=./libraries/SDL2_ttf/linux/lib64 -lSDL2_ttf
I_SDL2_TTF=-I"./libraries/SDL2_ttf/linux/include" -iquote"./libraries/SDL2/linux/include/SDL2"
LIBS=$(LIB_SDL2)
INCLUDES=$(I_SDL2)
CPPC=g++
CFLAGS= -std=c++11
OPT_RELEASE= -O3
OPT_DEBUG= -O0 -ggdb3 -Wall
DEBUG_FLAGS= -DDEBUG $(OPT_DEBUG)
RELEASE_FLAGS= -DNDEBUG $(OPT_RELEASE)

all: release debug
release: SDL2-skeleton-test
debug: SDL2-skeleton-test_d

###############
### release ###

SDL2-skeleton-test: skeleton-test.cpp bones.o environment.o sdlwrap.o textrenderer.o TGMath.a makestring.o ui.o Makefile
	@echo making SDL2-skeleton-test
	$(CPPC) skeleton-test.cpp bones.o environment.o  sdlwrap.o textrenderer.o TGMath.a makestring.o ui.o $(CFLAGS) $(INCLUDES) $(LIBS) $(RELEASE_FLAGS) -o "SDL2-skeleton-test" $(I_SDL2_TTF) $(LIB_SDL2_TTF)
	@echo --- --- ---
bones.o: bones.cpp bones.h Makefile
	@echo making bones.o
	$(CPPC) bones.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(RELEASE_FLAGS) -c -o "bones.o"
	@echo --- --- ---
environment.o: environment.cpp environment.h Makefile
	@echo making environment.o
	$(CPPC) environment.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(RELEASE_FLAGS) -c -o "environment.o" $(I_SDL2_TTF) $(LIB_SDL2_TTF)
	@echo --- --- ---
sdlwrap.o: sdlwrap.cpp sdlwrap.h Makefile
	@echo making sdlwrap.o
	$(CPPC) sdlwrap.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(RELEASE_FLAGS) -c -o "sdlwrap.o" $(I_SDL2_TTF) $(LIB_SDL2_TTF)
	@echo --- --- ---
textrenderer.o: textrenderer.cpp textrenderer.h Makefile
	@echo making textrenderer.o
	$(CPPC) textrenderer.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(RELEASE_FLAGS) -c -o "textrenderer.o" $(I_SDL2_TTF) $(LIB_SDL2_TTF)
	@echo --- --- ---
TGMath.a: Matrix4.o Quaternion.o Vec2.o Vec3.o MathTools.o Makefile
	@echo making TGMath.a
	ar -cvq TGMath.a Matrix4.o Quaternion.o Vec2.o Vec3.o MathTools.o
	@echo --- --- ---
Matrix4.o: ./tgmath/Matrix4.cpp ./tgmath/Matrix4.h Makefile
	@echo making Matrix4.o
	$(CPPC) -c ./tgmath/Matrix4.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(RELEASE_FLAGS) -o "Matrix4.o"
	@echo --- --- ---
Quaternion.o: ./tgmath/Quaternion.cpp ./tgmath/Quaternion.h Makefile
	@echo making Quaternion.o
	$(CPPC) ./tgmath/Quaternion.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(RELEASE_FLAGS) -c -o "Quaternion.o"
	@echo --- --- ---
Vec2.o: ./tgmath/Vec2.cpp ./tgmath/Vec2.h Makefile
	@echo making Vec2.o
	$(CPPC) ./tgmath/Vec2.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(RELEASE_FLAGS) -c -o "Vec2.o"
	@echo --- --- ---
Vec3.o: ./tgmath/Vec3.cpp ./tgmath/Vec3.h Makefile
	@echo making Vec3.o
	$(CPPC) ./tgmath/Vec3.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(RELEASE_FLAGS) -c -o "Vec3.o"
	@echo --- --- ---
MathTools.o: ./tgmath/MathTools.cpp ./tgmath/MathTools.h Makefile
	@echo making MathTools.o
	$(CPPC) ./tgmath/MathTools.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(RELEASE_FLAGS) -c -o "MathTools.o"
	@echo --- --- ---
makestring.o: makestring.h makestring.cpp Makefile
	@echo making makestring.o
	$(CPPC) makestring.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(RELEASE_FLAGS) -c -o "makestring.o"
	@echo --- --- ---
ui.o: ui.h ui.cpp Makefile
	@echo making ui.o
	$(CPPC) ui.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(RELEASE_FLAGS) -c -o "ui.o" $(I_SDL2_TTF) $(LIB_SDL2_TTF)
	@echo --- --- ---
#############
### debug ###

SDL2-skeleton-test_d: skeleton-test.cpp bones_d.o environment_d.o sdlwrap_d.o textrenderer_d.o TGMath_d.a makestring_d.o ui_d.o Makefile
	@echo making SDL2-skeleton-test_d
	$(CPPC) skeleton-test.cpp bones_d.o environment_d.o sdlwrap_d.o textrenderer_d.o TGMath_d.a makestring_d.o ui_d.o $(CFLAGS) $(INCLUDES) $(LIBS) $(DEBUG_FLAGS) -o "SDL2-skeleton-test_d" $(I_SDL2_TTF) $(LIB_SDL2_TTF)
	@echo --- --- ---
bones_d.o: bones.cpp bones.h Makefile
	@echo making bones_d.o
	$(CPPC) bones.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(DEBUG_FLAGS) -c -o "bones_d.o"
	@echo --- --- ---
environment_d.o: environment.cpp environment.h Makefile
	@echo making environment_d.o
	$(CPPC) environment.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(DEBUG_FLAGS) -c -o "environment_d.o" $(I_SDL2_TTF) $(LIB_SDL2_TTF)
	@echo --- --- ---
sdlwrap_d.o: sdlwrap.cpp sdlwrap.h Makefile
	@echo making sdlwrap_d.o
	$(CPPC) sdlwrap.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(DEBUG_FLAGS) -c -o "sdlwrap_d.o" $(I_SDL2_TTF) $(LIB_SDL2_TTF)
	@echo --- --- ---
textrenderer_d.o: textrenderer.cpp textrenderer.h Makefile
	@echo making textrenderer_d.o
	$(CPPC) textrenderer.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(DEBUG_FLAGS) -c -o "textrenderer_d.o" $(I_SDL2_TTF) $(LIB_SDL2_TTF)
	@echo --- --- ---
TGMath_d.a: Matrix4_d.o Quaternion_d.o Vec2_d.o Vec3_d.o MathTools_d.o Makefile
	@echo making TGMath_d.a
	-rm TGMath_d.a
	ar -cvq TGMath_d.a Matrix4_d.o Quaternion_d.o Vec2_d.o Vec3_d.o MathTools_d.o
	@echo --- --- ---
Matrix4_d.o: ./tgmath/Matrix4.cpp ./tgmath/Matrix4.h Makefile
	@echo making Matrix4_d.o
	$(CPPC) -c ./tgmath/Matrix4.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(DEBUG_FLAGS) -o "Matrix4_d.o"
	@echo --- --- ---
Quaternion_d.o: ./tgmath/Quaternion.cpp ./tgmath/Quaternion.h Makefile
	@echo making Quaternion_d.o
	$(CPPC) ./tgmath/Quaternion.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(DEBUG_FLAGS) -c -o "Quaternion_d.o"
	@echo --- --- ---
Vec2_d.o: ./tgmath/Vec2.cpp ./tgmath/Vec2.h Makefile
	@echo making Vec2_d.o
	$(CPPC) ./tgmath/Vec2.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(DEBUG_FLAGS) -c -o "Vec2_d.o"
	@echo --- --- ---
Vec3_d.o: ./tgmath/Vec3.cpp ./tgmath/Vec3.h Makefile
	@echo making Vec3_d.o
	$(CPPC) ./tgmath/Vec3.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(DEBUG_FLAGS) -c -o "Vec3_d.o"
	@echo --- --- ---
MathTools_d.o: ./tgmath/MathTools.cpp ./tgmath/MathTools.h Makefile
	@echo making MathTools_d.o
	$(CPPC) ./tgmath/MathTools.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(DEBUG_FLAGS) -c -o "MathTools_d.o"
	@echo --- --- ---
makestring_d.o: makestring.h makestring.cpp Makefile
	@echo making makestring_d.o
	$(CPPC) makestring.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(DEBUG_FLAGS) -c -o "makestring_d.o"
	@echo --- --- ---
ui_d.o: ui.h ui.cpp Makefile
	@echo making ui_d.o
	$(CPPC) ui.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(DEBUG_FLAGS) -c -o "ui_d.o" $(I_SDL2_TTF) $(LIB_SDL2_TTF)
	@echo --- --- ---


clean:
	-rm SDL2-skeleton-test*
	-rm *.o
	-rm *.a

.PHONY: all clean