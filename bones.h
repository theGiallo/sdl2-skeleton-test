#pragma once

#include "common_types.h"
#include "sdlwrap.h"

#include "tgmath/Matrix4.h"
#include "tgmath/Vec2.h"

#include <SDL2/SDL.h>

#include <string>
#include <vector>
#include <deque>
#include <map>
#include <ostream>
#include <array>
#include <set>
#include <memory>


/**
 * A bone is a line segment defined by a starting point, relative to his parent 
 * end point (i.e. origin for the root and for BONE_ABSOLUTE_POSITION flagged),
 * an angle expressed in degrees with classic trigonometry orientation (ccw from 
 * (1,0))) and a length. The rotation given by the angle is accumulated, or 
 * absolute if there is the flag BONE_ABSOLUTE_ANGLE.
 *
 * Each bone has his set of animations.
 * An animation is defined by a name and a set of keyframes, relative to the
 * single bone.
 * A keyframe is defined by his start time, relative to the animation, and a set
 * of bone properties.
 * Each bone has a reference to the active animation, the last 0 time and the 
 * IDs of the previous and next keyframe (maybe thay should moved into the 
 * animation, for now there is no need to do so).
 * 
 * The keyframes of the animation must be time growing, i.e. 
 * 
 *						kf[i].time<kf[i+1].time
 * 
 * The first keyframe can have time>0, if so the animation will do nothing until
 * the time of 1st frame.
 * There is no interpolation between the last and the 1st frame, so if you want 
 * to make a loopable animation you have to set the 1st frame time to 0 and make 
 * its properties equal to the last frame properties.
 * 
 **/
class Bone
{
public:
	enum ColorsID
	{
		SELECTED,
		CANDIDATE,
		DEFAULT,
		COLORS_COUNT
	};

protected:
	// Attributes
	//---
	static TGMath::f::Matrix4Stack* transform; // used to accumulate transforms

	static std::array<ColorRGBA, Bone::COLORS_COUNT> initColors();

public:
	// Attributes
	//---
	std::string name; // Just for the sake of the example
	struct BoneProperties
	{
		float x,		  // Starting point x
		      y,		  // Starting point y
		      a,		  // Angle, in degrees
		      l;		  // Length of the bone
	} properties;
	// struct Segment
	// {
		TGMath::f::Vec2 v0,v1; // updated in the draw call
	// } segment;

	Uint8 flags;	  // Bone flags, 8 bits should be sufficient for now
	
	std::vector<Bone*> children;   // Pointers to children
	Bone* parent;	 // Parent bone

	typedef struct
	{
		Time time;
		struct BoneProperties properties;
	} Keyframe;
	typedef struct 
	{
		std::string name;
		std::vector<std::shared_ptr<Keyframe>> keyframes;

		Time getLength() const;
	} Animation;
	std::map<std::string, Animation> animations;
	Animation* active_animation;
	bool animating;
protected:
	int pkf_id; // id of the previous keyframe
	int nkf_id; // id of the next keyframe
	Time last_zero; // last time animation passed through 0 time
	Time start_time, last_game_time;
	Time pkf_time; // time of the previous keyframe expressed in game time
	Time nkf_time; // time of the next keyframe expressed in game time

	void resetTime(Time t);
public:
	ColorsID color_id;


	static const Uint8 MAX_CHILDREN = 8;

	// Flags
	//---
	static const uint8_t BONE_ABSOLUTE_ANGLE =	  0x01; // Bone angle is absolute or relative to parent
	static const uint8_t BONE_ABSOLUTE_POSITION =   0x01<<1; // Bone position is absolute in the world or relative to the parent
	static const uint8_t BONE_ABSOLUTE = (BONE_ABSOLUTE_ANGLE | BONE_ABSOLUTE_POSITION);

	static Bone* selected;
	// TODO move the use of selected_id to a public function and privatize it
	static int selected_id; // id of the selected bone, for editing
	static std::deque<Bone*> bones;
	static std::array<ColorRGBA, COLORS_COUNT> colors;
	static ColorsID color_id_in_use;

	// Constructors
	//---
	Bone(std::string name, BoneProperties properties, Uint8 flags, Bone* parent);

	//Destructors
	//---
	~Bone();

	// Methods
	//---
	Bone& addChild(Bone* child); // add the child and returns its reference
	void dumpTree(std::ostream& os, int level=0); // prints this node and the subtree into the os
	void drawTree(SDL_Renderer* renderer); // renders this bone and all the subtree

	// TODO use integer ID
	void startAnimation(const std::string& name, Time game_time);
	void animate(Time game_time);
	bool hardSwitchAnimation(const std::string& name, Time game_time=-1, Time relative_time=-1); /// if is animating switches the active animation
	void stopAnimation();

	bool addKeyframeNow();

	void animateAtRelativeTime(Time relative);

	Time getRelativeTime(Time game_time) const;
	Time getRelativeTime() const; // relative to the last_game_time

	void angDec(); // decrements the angle of 1
	void angInc(); // increments the angle of 1
	void lenDec(); // decrements the length of 1
	void lenInc(); // increments the length of 1

	static void init(); // IMPORTANT! Call this before you use the class!
	static void selectNext();
	static void selectPrev();
	static void selectNone();
	static void selectBone(Bone* b);
	static void animateAll(Time game_time); // calls the animate method on all bones
	static Bone* getNearestBone(const TGMath::i::Vec2& p, TGMath::f::Vec2* out_P_on_bone, float* out_t); // out params can be nullptr; if there are no bones returns nullptr
}; // Bone

class Skeleton
{
public:
	Bone* root;
	std::set<std::string> animations;
	std::deque<Bone*> bones;

	Skeleton();

	const std::set<std::string>& getAnimations() const;
	const std::deque<Bone*>& getBones() const;

	void updateBonesList();
	void updateAnimationsList();

	bool hasAnimation(const std::string& name) const;
	void startAnimation(const std::string& name, Time game_time);
	void animate(Time game_time);
	void hardSwitchAnimation(const std::string& name, Time game_time=-1, Time relative_time=-1);
	void stopAnimation();
	void animateAtRelativeTime(Time relative);

	Time getMaxLength(const std::string& animation_name) const;
	Time getMaxRelativeTime() const;
}; // Skeleton