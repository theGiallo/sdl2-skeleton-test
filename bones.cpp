#include "bones.h"
#include "environment.h"

#include "sdlwrap.h"
#include "tgmath/MathTools.h"
#include "makestring.h"

#include <cfloat>
#include <cassert> // define NDEBUG before this inclusion to disable asserts
#include <queue>
#ifdef DEBUG
#include <iostream>
#endif

// Attributes
//---
TGMath::f::Matrix4Stack* Bone::transform = nullptr;
Bone* Bone::selected = nullptr;
std::deque<Bone*> Bone::bones = std::deque<Bone*>();
int Bone::selected_id = -1;
std::array<ColorRGBA, Bone::COLORS_COUNT> Bone::colors = Bone::initColors();
std::array<ColorRGBA, Bone::COLORS_COUNT> Bone::initColors()
{
	Bone::colors[Bone::SELECTED]  = {255, 77, 77, 255};   // red
	Bone::colors[Bone::CANDIDATE] = {255, 145, 77, 255};  // orange
	Bone::colors[Bone::DEFAULT]   = {179, 179, 179, 255}; // gray
	return Bone::colors;
}
Bone::ColorsID Bone::color_id_in_use = Bone::COLORS_COUNT;

// Constructors
//---
Bone::Bone(std::string name, BoneProperties properties, Uint8 flags, Bone* parent)
          : name(name),
            properties(properties),
            flags(flags),
            parent(parent),
            active_animation(nullptr),
            color_id(Bone::DEFAULT),
            animating(false)
{
	children.reserve(MAX_CHILDREN);
	#ifdef DEBUG
		std::cout<<"children.capacity="<<children.capacity()<<std::endl;
	#endif
	Bone::bones.push_back(this);
}

// Destructors
//---
Bone::~Bone()
{
	#ifdef DEBUG
	std::cout<<"~Bone() ["<<name<<"]"<<std::endl;
	#endif
	for (Bone* c: children)
	{
		delete c;
	}
	std::deque<Bone*>::iterator it;
	for (it = Bone::bones.begin(); it!=Bone::bones.end() && *it!=this; ++it);
	Bone::bones.erase(it);
}

// Methods
//---

// protected
//---
void Bone::resetTime(Time t)
{
	if (active_animation==nullptr)
	{
		return;
	}
	std::cout<<"bone "<<name<<" active_animation="<<active_animation<<std::endl;
	start_time = last_zero = t;
	pkf_id = 0;
	nkf_id = 0;
	pkf_time = nkf_time = last_zero + active_animation->keyframes[0]->time;
}

// public
//---
Bone& Bone::addChild(Bone* child)
{
	assert( children.size() <= Bone::MAX_CHILDREN );

	child->parent = this;
	children.push_back(child);
	return *child;
}
void Bone::dumpTree(std::ostream& os, int level)
{
	for (Bone* c: children)
	{
		c->dumpTree(os, level+1);
	}

	for (int i=0; i!=level; i++)
	{
		os<<"  ";
	}
	os<<name<<" "<<properties.x<<", "<<properties.y<<" "<<properties.a<<" "<<properties.l<<std::endl;
}
void Bone::drawTree(SDL_Renderer* renderer)
{
	static int ret;
	bool reset_color = false;
	static ColorRGBA color;
	static int level = -1;

	level++;

	transform->push();

	TGMath::f::Matrix4 tmp[2];
	*transform->current =
	(*transform->current)
	*TGMath::f::Matrix4::translationMatrix(TGMath::f::Vec3(properties.x,properties.y,0),tmp[0])
	*TGMath::f::Matrix4::rotationMatrix(TGMath::f::Vec3(0,0,properties.a),tmp[1]);

	TGMath::f::Vec3 start = *transform->current*TGMath::f::Vec3(0,0,0);
	TGMath::f::Vec3 end = *transform->current*TGMath::f::Vec3(properties.l,0,0);

	*transform->current =
	(*transform->current)
	*TGMath::f::Matrix4::translationMatrix(TGMath::f::Vec3(properties.l,0,0),tmp[0]);

	int ww,wh;
	SDL_GetWindowSize(Environment::window,
	                  &ww,
	                  &wh);

	// Set the color
	//---
	if (color_id_in_use!=color_id)
	{
		setRenderColor(renderer, colors[color_id]);
		color_id_in_use = color_id;

	}
	if (level==0)
	{
		getRenderColor(renderer, color);
		reset_color = true;
	}

	v0.x = start.x;
	v0.y = start.y;
	v1.x = end.x;
	v1.y = end.y;

	// Draw a line
	//---
	ret =
	SDL_RenderDrawLine(
	                    renderer, // SDL_Renderer* renderer: the renderer in which draw
						v0.x,	 // int x1: x of the starting point
						wh - v0.y,	 // int y1: y of the starting point
						v1.x,	 // int x2: x of the end point
						wh - v1.y);	// int y2: y of the end point
	DCHECKSDLERROR(ret);

	// Draw the subtree
	//---
	for (Bone* c: children)
	{
		c->drawTree(renderer);
	}

	if (reset_color)
	{
		setRenderColor(renderer,color);
	}
	if (level==0)
	{
		color_id_in_use = ColorsID::COLORS_COUNT;
	}

	transform->pop();

	level--;
}
void Bone::startAnimation(const std::string& name, Time game_time)
{
	Animation* anim;
	try {

		anim = & animations.at(name);

	} catch(std::out_of_range e)
	{
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "trying to start animation \"%s\" that does not exist", name.c_str());
		return;
	}
	active_animation = anim;
	animating = true;

	resetTime(game_time);

}
void Bone::animate(Time game_time)
{
	if (active_animation==nullptr || !animating)
		return;
	
	last_game_time = game_time;

	if (pkf_time > game_time) // first-frame time can be != 0
		return;
	
	std::vector<std::shared_ptr<Keyframe>>& kfs = active_animation->keyframes;

	int i=0;
	while (nkf_time < game_time)
	{
		i++;
		nkf_id++;
		if (nkf_id == kfs.size())
		{
			nkf_id = 0;
			last_zero = nkf_time;
		}
		nkf_time = last_zero + kfs[nkf_id]->time;
	}

	pkf_id = nkf_id-1;
	if (pkf_id == -1)
	{
		pkf_id += active_animation->keyframes.size();
	}
	Time dt = nkf_id!=0 ?
						 kfs[nkf_id]->time - kfs[pkf_id]->time
						:
						 kfs[nkf_id]->time;
	pkf_time = nkf_time - dt;

	float alpha = dt==0?1:(game_time - pkf_time) / dt;

	TGMath::linearInterpolationV<float,4>(&kfs[pkf_id]->properties.x, &kfs[nkf_id]->properties.x, &properties.x, 1-alpha);
}
bool Bone::hardSwitchAnimation(const std::string& name, Time game_time, Time relative_time)
{
	// if (active_animation==nullptr)
	// 	return false;

	Animation* anim;
	try {
		
		anim = & animations.at(name);

		if (relative_time == -1)
		{
			pkf_time = nkf_time = start_time;
		} else
		{
			pkf_time = nkf_time = start_time = game_time - relative_time;
			last_game_time = game_time;
		}
		last_zero = start_time;
		pkf_id = nkf_id = 0;

	} catch(std::out_of_range e)
	{
		stopAnimation();
		active_animation = nullptr;
		std::string s = tG::ms<<"trying to start animation \""<<name<<"\" that does not exist";
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, s.c_str());
		// throw std::runtime_error(s);
		return false;
	}
	// if (active_animation==nullptr)
	// 	startAnimation(name, start_time);
	// else
		active_animation = anim;
	if (!animating)
	{
		animating = true;
		animate(last_game_time);
		animating = false;
	}
	return true;
}
void Bone::stopAnimation()
{
	// active_animation = nullptr;
	animating = false;
}

bool Bone::addKeyframeNow()
{
	if (active_animation==nullptr)
	{
		return false;
	}
	Bone::Keyframe *kf = new Bone::Keyframe({getRelativeTime(), properties});
	// std::shared_ptr<Bone::Keyframe> nkf = std::shared_ptr<Bone::Keyframe>(kf);
	active_animation->keyframes.insert(active_animation->keyframes.begin()+nkf_id, std::shared_ptr<Bone::Keyframe>(kf));

	return true;
}

void Bone::animateAtRelativeTime(Time relative)
{
	resetTime(0);
	animate(relative);
}
Time Bone::getRelativeTime(Time game_time) const
{
	return game_time - last_zero;
}
Time Bone::getRelativeTime() const
{
	return last_game_time - last_zero;
}

void Bone::angInc()
{
	Bone::selected->properties.a++;
	if (Bone::selected->properties.a>=360)
	{
		Bone::selected->properties.a -= 360;
	}
}
void Bone::angDec()
{
	Bone::selected->properties.a--;
	if (Bone::selected->properties.a<=0)
	{
		Bone::selected->properties.a +=360;
	}
}
void Bone::lenInc()
{
	Bone::selected->properties.l++;
}
void Bone::lenDec()
{
	Bone::selected->properties.l--;
	if (Bone::selected->properties.l<0)
	{
		Bone::selected->properties.l = -Bone::selected->properties.l;
		Bone::selected->properties.a += 180;
		if (Bone::selected->properties.a>=360)
		{
			Bone::selected->properties.a -= 360;
		}
	}
}

// Static Methods
//---
void Bone::init()
{
	if (transform!=nullptr)
	{
		delete transform;
	}
	transform = new TGMath::f::Matrix4Stack();
}
void Bone::selectNext()
{
	if (selected==nullptr)
	{
		selected_id = 0;
	} else
	{
		selected->color_id = Bone::DEFAULT;
		selected_id++;
		if (selected_id == bones.size())
		{
			selected_id = 0;
		}
	}
	selected = bones[selected_id];
	selected->color_id = Bone::SELECTED;
	std::cout<<"selected is "<<selected->name<<std::endl;
}
void Bone::selectPrev()
{
	if (selected==nullptr)
	{
		selected_id = bones.size()-1;
	} else
	{
		selected->color_id = Bone::DEFAULT;
		selected_id--;
		if (selected_id == -1)
		{
			selected_id = bones.size()-1;
		}
	}
	selected = bones[selected_id];
	selected->color_id = Bone::SELECTED;
	std::cout<<"selected is "<<selected->name<<std::endl;
}
void Bone::selectNone()
{
	selected = nullptr;
}
void Bone::selectBone(Bone* b)
{
	if (Bone::selected!=nullptr)
		Bone::selected->color_id = Bone::DEFAULT;
	Bone::selected = b;
	if (b==nullptr)
	{
		Bone::selected_id = -1;
		return;
	}
	Bone::selected->color_id = Bone::SELECTED;
	int id=0;
	for ( id=0; id!=Bone::bones.size() && Bone::bones[id]!=Bone::selected; id++)
	{}
	Bone::selected_id = id;
}

void Bone::animateAll(Time game_time)
{
	for (Bone* b: bones)
	{
		b->animate(game_time);
	}
}

Bone* Bone::getNearestBone(const TGMath::i::Vec2& p, TGMath::f::Vec2* out_P_on_bone, float* out_t)
{
	float d,t,mint,mind = FLT_MAX;
	TGMath::f::Vec2 pos, minpos;
	Bone* res = nullptr;

	for (Bone* b: bones)
	{
		d = TGMath::f::distFromSegment(p, b->v0, b->v1, &pos, &t);
		if (d < mind || (d==mind && pos==b->v0))
		{
			res = b;
			mind = d;
			minpos = pos;
			mint = t;
		}
	}

	if (out_P_on_bone!=nullptr)
	{
		*out_P_on_bone = minpos;
	}
	if (out_t!=nullptr)
	{
		*out_t = mint;
	}

	return res;
}

// Animation struct
//---
Time Bone::Animation::getLength() const
{
	return keyframes[keyframes.size()-1]->time;
}

// Skeleton Class
//---
Skeleton::Skeleton() : root(nullptr),
					   animations(std::set<std::string>()),
					   bones(std::deque<Bone*>())
{}
const std::set<std::string>& Skeleton::getAnimations() const
{
	return animations;
}
const std::deque<Bone*>& Skeleton::getBones() const
{
	return bones;
}
void Skeleton::updateBonesList()
{
	bones.empty();
	std::queue<Bone*> to_visit;
	to_visit.push(root);

	while (to_visit.size())
	{
		Bone* b = to_visit.front();
		to_visit.pop();
		bones.push_back(b);
		std::cout<<"visiting "<<b->name<<std::endl;
		for (Bone* bc : b->children)
		{
			to_visit.push(bc);
		}
	}
}
void Skeleton::updateAnimationsList()
{
	animations.empty();
	for (Bone* b : bones)
	{
		std::cout<<"animations of "<<b->name<<std::endl;
		for (std::pair<const std::string, Bone::Animation>& na : b->animations)
		{
			std::cout<<"inserting "<<na.second.name<<std::endl;
			animations.insert(na.second.name);
		}
	}
}

bool Skeleton::hasAnimation(const std::string& name) const
{
	auto search = animations.find(name);
	if(search != animations.end())
	{
		return true;
	}
	return false;
}
void Skeleton::startAnimation(const std::string& name, Time game_time)
{
	for (Bone* b : bones)
		b->startAnimation(name, game_time);
}
void Skeleton::animate(Time game_time)
{
	for (Bone* b : bones)
		b->animate(game_time);
}
void Skeleton::hardSwitchAnimation(const std::string& name, Time game_time, Time relative_time)
{
	for (Bone* b : bones)
		b->hardSwitchAnimation(name, game_time, relative_time);
}
void Skeleton::stopAnimation()
{
	for (Bone* b : bones)
		b->stopAnimation();
}
void Skeleton::animateAtRelativeTime(Time relative)
{
	for (Bone* b : bones)
		b->animateAtRelativeTime(relative);
}
Time Skeleton::getMaxLength(const std::string& animation_name) const
{
	Time max = 0, l;
	for (Bone*b : bones)
	{
		try{
			if ( (l = b->animations.at(animation_name).getLength()) >= max )
				max = l;
		} catch (std::out_of_range e)
		{
			SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "animation \"%s\" that does not exist in bone %s", animation_name.c_str(), b->name.c_str());
		}
	}
	return max;
}
Time Skeleton::getMaxRelativeTime() const
{
	Time max = 0, l;
	for (Bone*b : bones)
	{
		if ( b->active_animation!=nullptr && (l = b->getRelativeTime()) >= max )
			max = l;
	}
	return max;
}