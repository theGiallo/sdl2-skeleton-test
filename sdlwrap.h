#pragma once

#include "environment.h"
#include <SDL2/SDL.h>

#ifdef DEBUG
#define DCHECKSDLERROR(ret) if (ret != 0)\
	{\
		const char *error = SDL_GetError();\
		if (*error != '\0')\
		{\
			SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "SDL error: %s at line #%d of file %s", error, __LINE__, __FILE__);\
			SDL_ClearError();\
		}\
	}
#else
#define DCHECKSDLERROR(ret)
#endif

using ColorRGBA = struct ColorRGBA_st
{
	Uint8 r,g,b,a;
	operator SDL_Color();
};

void getRenderColor(SDL_Renderer* renderer, ColorRGBA& color);
void setRenderColor(SDL_Renderer* renderer, const ColorRGBA& color);

using SDLWrap_Texture = struct SDLWrap_Texture_st
{
	SDL_Texture* texture;
	SDL_Rect size;
};

bool renderTexturedRect(const SDLWrap_Texture& texture, const SDL_Rect& rect,  const ColorRGBA& color = {255,255,255,255});
bool renderRectBorder(const SDL_Rect& rect, const ColorRGBA& color);
bool renderRectPlain(const SDL_Rect& rect, const ColorRGBA& color);