/**
 * Some experiments about skeletal animation and ragdolls
 * by Gianluca Alloisio a.k.a. theGiallo
 * forked 2014/06/15
 * --------------------------------------------------------------------------------
 *
 **/

#include <SDL2/SDL.h>
#include <SDL2/SDL_video.h>
#include <SDL_ttf.h>
#include <iostream>
#include <utility> // for std::move
#include <sstream>
#include <cassert>

#include "tgmath/MathConsts.h"
#include "tgmath/MathTools.h"
#include "tgmath/Vec2.h"
#include "bones.h"
#include "environment.h"
#include "textrenderer.h"
#include "makestring.h"
#include "ui.h"

// Consts for the frame independent game loop
//---
const double MAX_FPS = 120.0;
const double MIN_FPS = 16.0;
const double EXPECTED_FPS = 60.0;
const double MAX_UPS = 300.0;
const double FIXED_TIMESTEP = 1.0 / MAX_UPS;
const double FIXED_TIMESTEP_2 = FIXED_TIMESTEP * FIXED_TIMESTEP;
const double MIN_TIME = 1.0 / MAX_FPS;
const double MAX_TIME = 1.0 / MIN_FPS;


struct Color
{
	Uint8 r,g,b,a;
	Color(Uint8 r=255,Uint8 g=255,Uint8 b=255,Uint8 a=255) : r(r),g(g),b(b),a(a){}
	void makeInterpolationOf(Color& a, Color& b, float alpha)
	{
		TGMath::linearInterpolationRoundV<Uint8,4>(static_cast<Uint8*>(&a.r),static_cast<Uint8*>(&b.r),static_cast<Uint8*>(&r),alpha);
	}
};
struct PixPos
{
	int x,y;
	PixPos(int x=0, int y=0) : x(x),y(y){}
	void makeInterpolationOf(PixPos& a, PixPos& b, float alpha)
	{
		TGMath::linearInterpolationRoundV<int,2>(static_cast<int*>(&a.x),static_cast<int*>(&b.x),static_cast<int*>(&x),alpha);
	}
};
struct Aspect
{
	Color color;
	PixPos pixel_position;
	Aspect(){};
	Aspect(Color&& _color, PixPos&& _pixel_position) : color(std::move(_color)), pixel_position(std::move(_pixel_position)) {};
	void makeInterpolationOf(Aspect& a, Aspect& b, float alpha)
	{
		color.makeInterpolationOf(a.color, b.color, alpha);
		pixel_position.makeInterpolationOf(a.pixel_position, b.pixel_position, alpha);
	}
};
struct PhysicsProperties
{
	struct Pos {float x,y;} pos;
	struct Vel {float x,y;} vel;
	struct Acc {float x,y;} acc;
	PhysicsProperties() : pos({0,0}), vel({0,0}), acc({0,0}){}
	PhysicsProperties(Pos& pos, Vel& vel, Acc& acc) : pos(pos), vel(vel), acc(acc){}
	PhysicsProperties(Pos&& _pos, Vel&& _vel, Acc&& _acc) : pos(std::move(_pos)), vel(std::move(_vel)), acc(std::move(_acc)){}
	void makeInterpolationOf(PhysicsProperties& a, PhysicsProperties& b, float alpha)
	{
		TGMath::linearInterpolationV<float,4>(static_cast<float*>(&a.pos.x),static_cast<float*>(&b.pos.x),static_cast<float*>(&pos.x),alpha);
	}
	float totalAcc(Uint8 index)
	{
		// here we will calculate the total acceleration affecting the object; for example gravity, for now nothing.
		// (this probably could be function of pos, if so we have to separate the for cycle in stepAhead(...) )
		return 0;
	}
	// leapfrog integration
	void stepAhead(PhysicsProperties& prev_state)
	{
		// separate this cycle if totalAcc is in function of pos
		for (Uint8 i=0; i!=2; i++)
		{
			(&pos.x)[i] = (&prev_state.pos.x)[i] + (&prev_state.vel.x)[i]*FIXED_TIMESTEP + 0.5f*(&prev_state.acc.x)[i]*FIXED_TIMESTEP_2;
			(&acc.x)[i] = totalAcc(i);
			(&vel.x)[i] = (&prev_state.vel.x)[i] + 0.5f*((&acc.x)[i]+(&prev_state.acc.x)[i])*FIXED_TIMESTEP;
		}
	}
};

// an example of a working structure using the above ones
struct Square
{
	PhysicsProperties pp[3];
	Aspect aspect; // if aspect is in function of time or it's static we don't have to interpolate
	int px_side;
	// you should not touch these, so they should be made private and accessed through a method
	Uint8 currID;
	Uint8 prevID;
	const Uint8 renderID = 2;

	Square() : px_side(10), currID(0), prevID(1){}

	void fixedTSUpdate(double game_time)
	{
		std::swap(currID, prevID);
		pp[currID].stepAhead(pp[prevID]);
	}

	void smoothToRenderState(float alpha)
	{
		pp[renderID].makeInterpolationOf(pp[currID],pp[prevID],alpha);
	}

	void render(SDL_Renderer* renderer)
	{
		for (Uint8 i=0; i!=2; i++)
		{
			(&aspect.pixel_position.x)[i] = std::round((&pp[renderID].pos.x)[i]);
		}

		// Declare and initialize our rectangle
		SDL_Rect rect = {
						aspect.pixel_position.x, aspect.pixel_position.y,   // int x,y: position of the top left corner
						px_side, px_side};                                  // int w,h: width and height

		// set the color to draw with
		SDL_SetRenderDrawColor(
							renderer,        // SDL_Renderer* renderer: the renderer to affect
							aspect.color.r,  // Uint8 r: Red
							aspect.color.g,  // Uint8 g: Green
							aspect.color.b,  // Uint8 b: Blue
							aspect.color.a); // Uint8 a: Alpha

		int ret;
		ret = SDL_RenderFillRect(
								renderer,    // SDL_Renderer*   renderer: the renderer to affect
								   &rect);   // const SDL_Rect* rect: the rectangle to fill);
		if (ret != 0)
		{
			const char *error = SDL_GetError();
			if (*error != '\0')
			{
				SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not draw a rect. SDL Error: %s at line #%d of file %s", error, __LINE__, __FILE__);
				SDL_ClearError();
			}
		}
	}

	void update(double game_time, double delta_time){};
};


double getTimeInSec()
{
	static double coeff;
	static bool isInitialized;
	if (!isInitialized) {
		isInitialized = true;
		Uint64 freq = SDL_GetPerformanceFrequency();
		coeff = 1.0 / (double)freq;
	}
	Uint64 val = SDL_GetPerformanceCounter();

	return (double)val * coeff;
}


int main(int argc, char** argv)
{
	// SDL version
	SDL_version SDL_compiled;
	SDL_version SDL_linked;

	SDL_VERSION(&SDL_compiled);
	SDL_GetVersion(&SDL_linked);
	printf("Compiled against SDL version %d.%d.%d\n",
		   SDL_compiled.major, SDL_compiled.minor, SDL_compiled.patch);
	printf("Linking against SDL version %d.%d.%d\n",
		   SDL_linked.major, SDL_linked.minor, SDL_linked.patch);

	// SDL_ttf version
	SDL_version SDL_ttf_compiled;
	const SDL_version *SDL_ttf_linked=TTF_Linked_Version();
	SDL_TTF_VERSION(&SDL_ttf_compiled);
	printf("Compiled against SDL_ttf version: %d.%d.%d\n", 
			SDL_ttf_compiled.major, SDL_ttf_compiled.minor, SDL_ttf_compiled.patch);
	printf("Linking against SDL_ttf version: %d.%d.%d\n", 
			SDL_ttf_linked->major, SDL_ttf_linked->minor, SDL_ttf_linked->patch);


#ifdef DEBUG
 	SDL_LogSetAllPriority(SDL_LOG_PRIORITY_VERBOSE);
#else
 	SDL_LogSetAllPriority(SDL_LOG_PRIORITY_WARN);
#endif

	int ret;

	// Initialize
	//---

	// Initialize SDL_ttf
	if(TTF_Init()==-1)
	{
		std::cerr<<"TTF_Init: "<<TTF_GetError()<<std::endl;
		return 1;
	}

	// Initialize SDL's Video subsystem
	ret = SDL_Init(SDL_INIT_VIDEO);
	if (ret != 0)
	{
		const char *error = SDL_GetError();
		if (*error != '\0')
		{
			SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not SDL_Init. SDL error: %s at line #%d of file %s", error, __LINE__, __FILE__);
			// SDL_ClearError();
			return 1;
		}
	}

	// Create an application window with the following settings:
	Environment::window = SDL_CreateWindow(
						"SDL2 Skeleton test",               // const char* title
						SDL_WINDOWPOS_UNDEFINED,            // int x: initial x position, SDL_WINDOWPOS_CENTERED, or SDL_WINDOWPOS_UNDEFINED
						SDL_WINDOWPOS_UNDEFINED,            // int y: initial y position, SDL_WINDOWPOS_CENTERED, or SDL_WINDOWPOS_UNDEFINED
						1024,                               // int w: width, in pixels
						576,                                // int h: height, in pixels
						SDL_WINDOW_SHOWN                    // Uint32 flags: window options, see docs
			);

	// Check that the window was successfully made
	if(Environment::window==NULL)
	{
		const char *error = SDL_GetError();
		if (*error != '\0')
		{
			// In the event that the window could not be made...
			SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not create window. SDL error: %s at line #%d of file %s", error, __LINE__, __FILE__);
			SDL_Quit();
			return 1;
		}
	}

	// Create accelerated renderer
	Environment::renderer = SDL_CreateRenderer(
	                              Environment::window,          // SDL_Window* window: the window where rendering is displayed
	                              -1,                           // int index: the index of the rendering driver to initialize, or -1 to initialize the first one supporting the requested flags
	                              SDL_RENDERER_ACCELERATED);    // Uint32 flags: 0, or one or more SDL_RendererFlags OR'd together;
	// Check that the renderer was successfully made
	if(Environment::renderer==NULL)
	{
		const char *error = SDL_GetError();
		if (*error != '\0')
		{
			// In the event that the window could not be made...
			SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not create accelerated renderer. SDL error: %s at line #%d of file %s", error, __LINE__, __FILE__);
			// Fallback to software renderer
			Environment::renderer = SDL_CreateRenderer(
			                              Environment::window,
			                              -1,
			                              SDL_RENDERER_SOFTWARE);
			if(Environment::renderer==NULL)
			{
				const char *error = SDL_GetError();
				if (*error != '\0')
				{
					// In the event that the window could not be made...
					SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not even create software renderer. SDL error: %s at line #%d of file %s", error, __LINE__, __FILE__);
					SDL_Quit();
					return 1;
				}
			}
			SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Fallback to software renderer, you may observe poor performance.");
		}
	}


	// Program life cycle
	//---

	SDL_Event event;
	bool running = true;
	Time old_time = getTimeInSec();
	Time game_time = 0, time_accumulator;
	double alpha;
	int n_steps=0;

	// Test Bone
	//---
	Bone::init();
	Skeleton sk;
	Bone bone0("Root",{200,300,0,50},0,nullptr);
	sk.root = &bone0;
	Bone& gen1 = bone0.addChild( new Bone("gen1",{0,0,-90,50},0,nullptr));
	Bone& gen2 = gen1.addChild( new Bone("gen2",{10,10,-90,50},0,nullptr));
	gen2.addChild( new Bone("gen3_0",{0,0,45,50},0,nullptr));
	gen2.addChild( new Bone("gen3_1",{0,0,-45,50},0,nullptr));

	bone0.dumpTree(std::cout);

	Bone::Animation ta0 = {"test_delay",
	                     	{
	                        	std::shared_ptr<Bone::Keyframe>(new Bone::Keyframe({1,{200,300,90,50}})),
	                        	std::shared_ptr<Bone::Keyframe>(new Bone::Keyframe({3,{200,300,-90,50}})),
	                        	std::shared_ptr<Bone::Keyframe>(new Bone::Keyframe({4,{200,300,-180,50}})),
	                        	std::shared_ptr<Bone::Keyframe>(new Bone::Keyframe({4,{200,300,180,50}})),
	                        	std::shared_ptr<Bone::Keyframe>(new Bone::Keyframe({5,{200,300,90,50}})),
	                        }
	                     };
	bone0.animations[ta0.name] = ta0;

	Bone::Animation ta1 = {"test_0delay",
	                     	{
	                        	std::shared_ptr<Bone::Keyframe>(new Bone::Keyframe({0,{200,300,90,50}})),
	                        	std::shared_ptr<Bone::Keyframe>(new Bone::Keyframe({2,{200,300,-90,50}})),
	                        	std::shared_ptr<Bone::Keyframe>(new Bone::Keyframe({3,{200,300,-180,50}})),
	                        	std::shared_ptr<Bone::Keyframe>(new Bone::Keyframe({3,{200,300,180,50}})),
	                        	std::shared_ptr<Bone::Keyframe>(new Bone::Keyframe({3,{200,300,180,50}})),
	                        	std::shared_ptr<Bone::Keyframe>(new Bone::Keyframe({4,{200,300,90,50}})),
	                        }
	                     };
	bone0.animations[ta1.name] = ta1;

	Bone::Animation ta0_bone1 = {"test_0delay",
	                     	      {
	                            	std::shared_ptr<Bone::Keyframe>(new Bone::Keyframe({0,{0,0,20,50}})),
	                        		std::shared_ptr<Bone::Keyframe>(new Bone::Keyframe({1,{0,0,-20,50}})),
	                        		std::shared_ptr<Bone::Keyframe>(new Bone::Keyframe({2,{0,0,20,50}}))
	                              }
	                            };
	gen1.animations[ta0_bone1.name] = ta0_bone1;

	Bone::Animation ta0_bone2 = {"test_0delay",
	                     	      {
	                            	std::shared_ptr<Bone::Keyframe>(new Bone::Keyframe({0,{0,0,110,50}})),
	                        		std::shared_ptr<Bone::Keyframe>(new Bone::Keyframe({0.5,{0,0,70,50}})),
	                        		std::shared_ptr<Bone::Keyframe>(new Bone::Keyframe({1,{0,0,110,50}}))
	                              }
	                            };
	gen2.animations[ta0_bone2.name] = ta0_bone2;

	sk.updateBonesList();
	sk.updateAnimationsList();

	std::string selected_animation_name = ta1.name;
	std::shared_ptr<Bone::Keyframe> selected_keyframe = nullptr;

	// Editor
	//---
	bool edit_mode = false,
	     rotating = false,
	     scaling = false,
	     translating = false,
	     scale_mode = false,
	     rotate_mode = false,
	     translate_mode = false,
	     view_to_update = false;
	TGMath::i::Vec2 prev_mouse_p;
	Bone* nearest_bone = nullptr;
	TGMath::f::Vec2 pol;
	TGMath::i::Vec2 mouse_pos;
	TGMath::f::Vec2 scale_V, scale_pos_dir, translate_V;
	float scaling_dist;

	// UI
	//---
	tG::UI::init();
	tG::UI::ConstDropDown animations_dd = tG::UI::ConstDropDown(sk.animations);
	{
	int ww,wh;
	SDL_GetWindowSize(Environment::window,&ww,&wh);
	animations_dd.pos = {16,wh-16};
	animations_dd.setWidth(100);
	animations_dd.setHeight(24);
	animations_dd.setMinWidth(62);
	animations_dd.setMinHeight(16);
	animations_dd.setMaxWidth(150);
	animations_dd.setMaxHeight(32);
	animations_dd.setFlag(tG::UI::ConstDropDown::FIXED_HEIGHT);
	animations_dd.onSelectCallback = [&selected_animation_name, &sk, &game_time] (uint selected, uint old, const std::vector<const std::string*> strings)
	{
		selected_animation_name = *strings[selected];
		sk.hardSwitchAnimation(selected_animation_name, game_time, sk.getMaxRelativeTime());
	};
	}
	tG::UI::AnimationTimeline animation_timeline;
	animation_timeline.setSkeleton(&sk);
	animation_timeline.setAnimation(selected_animation_name);
	animation_timeline.onSelectKeyframeCallback = [&selected_keyframe] (std::shared_ptr<Bone::Keyframe> k)
	{
		selected_keyframe = k;
	};



	Square sq;
	sq.pp[sq.currID].vel.x = 20;
	sq.pp[sq.currID].vel.y = -75;
	
	sq.pp[sq.currID].pos.x = 50;
	sq.pp[sq.currID].pos.y = 300;


	// Test for the textrenderer
	//---
	// Font f;
	// Font::getFont(f);
	// Font ff = Font::getFont(Font("./fonts/Inconsolata/Inconsolata-Regular.ttf", 16));
	// std::cout<<"---------- ---------- ----------"<<std::endl;
	// f = Font::getFont(Font(".", 16));
	// std::cout<<"---------- ---------- ----------"<<std::endl;
	// std::cout<<"f.font ="<<f.font<<" f.path ='"<<f.path<<"' f.ptsize ="<<f.ptsize<<std::endl;
	// f = Font::getFont(Font("./fonts/Inconsolata/Inconsolata-Regular.ttf", 32));
	// std::cout<<"---------- ---------- ----------"<<std::endl;
	// std::cout<<"f.font ="<<f.font<<" f.path ='"<<f.path<<"' f.ptsize ="<<f.ptsize<<std::endl;
	// std::cout<<"ff.font="<<ff.font<<" ff.path='"<<ff.path<<"' ff.ptsize="<<ff.ptsize<<std::endl;
	// std::cout<<"---------- ---------- ----------"<<std::endl;
	// std::cout<<"f.font ="<<f.font<<" f.path ='"<<f.path<<"' f.ptsize ="<<f.ptsize<<std::endl;
	// ff = Font::getFont(Font("./fonts/Inconsolata/Inconsolata-Regular.ttf", 32));
	// std::cout<<"---------- ---------- ----------"<<std::endl;
	// std::cout<<"f.font ="<<f.font<<" f.path ='"<<f.path<<"' f.ptsize ="<<f.ptsize<<std::endl;
	// std::cout<<"ff.font="<<ff.font<<" ff.path='"<<ff.path<<"' ff.ptsize="<<ff.ptsize<<std::endl;
	// std::cout<<"---------- ---------- ----------"<<std::endl;
	// std::cout<<"f.font ="<<f.font<<" f.path ='"<<f.path<<"' f.ptsize ="<<f.ptsize<<std::endl;
	// f = Font::getFont(Font("./fonts/Inconsolata/Inconsolata-Regular.ttf", -1));
	// std::cout<<"---------- ---------- ----------"<<std::endl;
	// std::cout<<"f.font ="<<f.font<<" f.path ='"<<f.path<<"' f.ptsize ="<<f.ptsize<<std::endl;
	// std::cout<<"ff.font="<<ff.font<<" ff.path='"<<ff.path<<"' ff.ptsize="<<ff.ptsize<<std::endl;

	// load font .ttf at size 16 into font
	TTF_Font *font;
	font = TTF_OpenFont("fonts/Inconsolata/Inconsolata-Regular.ttf",    // const char *file: File name to load font from.
	                                                            14);    // int ptsize: Point size (based on 72DPI) to load font as. This basically translates to pixel height.
	if(!font)
	{
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "TTF_OpenFont: %s\n", TTF_GetError());
		return 2;
	}

	// get the loaded font's style
	{
	int style;
	style = TTF_GetFontStyle(font);
	std::cout<<"The font style is:";
	if(style==TTF_STYLE_NORMAL)
		std::cout<<" normal";
	else {
		if(style&TTF_STYLE_BOLD)
			std::cout<<" bold";
		if(style&TTF_STYLE_ITALIC)
			std::cout<<" italic";
		if(style&TTF_STYLE_UNDERLINE)
			std::cout<<" underline";
		if(style&TTF_STYLE_STRIKETHROUGH)
			std::cout<<" strikethrough";
	}
	std::cout<<std::endl;
	}
	
	// set the loaded font's style to bold italics
	TTF_SetFontStyle(font, TTF_STYLE_BOLD|TTF_STYLE_ITALIC);

	// get the loaded font's style
	{
	int style;
	style = TTF_GetFontStyle(font);
	std::cout<<"The font style is:";
	if(style==TTF_STYLE_NORMAL)
		std::cout<<" normal";
	else
	{
		if(style&TTF_STYLE_BOLD)
			std::cout<<" bold";
		if(style&TTF_STYLE_ITALIC)
			std::cout<<" italic";
		if(style&TTF_STYLE_UNDERLINE)
			std::cout<<" underline";
		if(style&TTF_STYLE_STRIKETHROUGH)
			std::cout<<" strikethrough";
	}
	std::cout<<std::endl;
	}

	{
	// get the loaded font's outline width
	int outline=TTF_GetFontOutline(font);
	SDL_Log("The font outline width is %d pixels\n",outline);
	}
	
	// set the loaded font's outline to 1 pixel wide
	TTF_SetFontOutline(font, 1);
	
	{
	// get the loaded font's outline width
	int outline=TTF_GetFontOutline(font);
	SDL_Log("The font outline width is %d pixels\n",outline);
	}

	{
	// get the loaded font's hinting setting
	int hinting=TTF_GetFontHinting(font);
	SDL_Log("The font hinting is currently set to %s\n",
			hinting==TTF_HINTING_NORMAL?"Normal":
			hinting==TTF_HINTING_LIGHT?"Light":
			hinting==TTF_HINTING_MONO?"Mono":
			hinting==TTF_HINTING_NONE?"None":
			"Unknonwn");
	}

	// set the loaded font's hinting to optimized for monochrome rendering
	TTF_SetFontHinting(font, TTF_HINTING_MONO);
	// set the loaded font's hinting to optimized for monochrome rendering
	TTF_SetFontHinting(font, TTF_HINTING_NORMAL);

	{
	// get the loaded font's kerning setting
	int kerning = TTF_GetFontKerning(font);
	SDL_Log("The font kerning is currently set to %s\n",
			kerning==0?"Off":"On");
	}
	
	// turn off kerning on the loaded font
	TTF_SetFontKerning(font, 0);

	// turn on kerning on the loaded font
	TTF_SetFontKerning(font, 1);

	// get the loaded font's max height
	SDL_Log("The font max height is: %d\n", TTF_FontHeight(font));

	// get the loaded font's max ascent
	/**
	 * Get the maximum pixel ascent of all glyphs of the loaded font.
	 * This can also be interpreted as the distance from the top of the font to the baseline.
	 * It could be used when drawing an individual glyph relative to a top point, by combining it
	 * with the glyph's maxy metric to resolve the top of the rectangle used when blitting the glyph on the screen.
	 * 
	 * rect.y = top + TTF_FontAscent(font) - glyph_metric.maxy;
	 **/
	SDL_Log("The font ascent is: %d\n", TTF_FontAscent(font));

	// get the loaded font's max descent
	/**
	 * Get the maximum pixel descent of all glyphs of the loaded font.
	 * This can also be interpreted as the distance from the baseline to the bottom of the font.
	 * It could be used when drawing an individual glyph relative to a bottom point, by combining it
	 * with the glyph's maxy metric to resolve the top of the rectangle used when blitting the glyph on the screen.
	 *
	 * rect.y = bottom - TTF_FontDescent(font) - glyph_metric.maxy;
	 **/
	SDL_Log("The font descent is: %d\n", TTF_FontDescent(font));

	// get the loaded font's line skip height
	/**
	 * Get the recommended pixel height of a rendered line of text of the loaded font.
	 * This is usually larger than the TTF_FontHeight of the font.
	 **/
	SDL_Log("The font line skip is: %d\n", TTF_FontLineSkip(font));

	// get the loaded font's number of faces
	SDL_Log("The number of faces in the font is: %ld\n", TTF_FontFaces(font));

	// get the loaded font's face fixed status
	if(TTF_FontFaceIsFixedWidth(font))
		SDL_Log("The font is fixed width.\n");
	else
		SDL_Log("The font is not fixed width.\n");

	{
	// get the loaded font's face name
	char *familyname=TTF_FontFaceFamilyName(font);
	if(familyname)
		SDL_Log("The family name of the face in the font is: %s\n", familyname);
	}

	{
	// get the loaded font's face style name
	char *stylename=TTF_FontFaceStyleName(font);
	if(stylename)
		SDL_Log("The name of the face in the font is: %s\n", stylename);
	}

	{
	// check for a glyph for 'g' in the loaded font
	int index=TTF_GlyphIsProvided(font,'g');
	if(!index)
		SDL_Log("There is no 'g' in the loaded font!\n");
	}

	{
	// get the glyph metric for the letter 'g' in a loaded font
	int minx,maxx,miny,maxy,advance;
	if(TTF_GlyphMetrics(font,'g',&minx,&maxx,&miny,&maxy,&advance)==-1)
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "TTF_GlyphMetrics generated an error at line %d of file %s Error:%s\n",__LINE__,__FILE__,TTF_GetError());
	else
	{
		SDL_Log("minx    : %d\n",minx);
		SDL_Log("maxx    : %d\n",maxx);
		SDL_Log("miny    : %d\n",miny);
		SDL_Log("maxy    : %d\n",maxy);
		SDL_Log("advance : %d\n",advance);
	}
	}   

	{
	// get the width and height of a string as it would be rendered in a loaded font
	int w,h;
	if(TTF_SizeText(font,"Hello World!",&w,&h))
	{
		// perhaps print the current TTF_GetError(), the string can't be rendered...
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "TTF_SizeText generated an error at line %d of file %s Error:%s\n",__LINE__,__FILE__,TTF_GetError());
	} else
	{
		SDL_Log("width=%d height=%d\n",w,h);
	}
	}

	{
	// get the width and height of a string as it would be rendered in a loaded font
	int w,h;
	if(TTF_SizeUTF8(font,"Hello World!",&w,&h))
	{
		// perhaps print the current TTF_GetError(), the string can't be rendered...
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "TTF_SizeUTF8 generated an error at line %d of file %s Error:%s\n",__LINE__,__FILE__,TTF_GetError());
	} else
	{
		SDL_Log("width=%d height=%d\n",w,h);
	}
	}

	{
	// get the width and height of a string as it would be rendered in a loaded font
	int w,h;
	Uint16 text[]={'H','e','l','l','o',' ',
				   'W','o','r','l','d','!',0};
	if(TTF_SizeUNICODE(font,text,&w,&h))
	{
		// perhaps print the current TTF_GetError(), the string can't be rendered...
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "TTF_SizeUNICODE generated an error at line %d of file %s Error:%s\n",__LINE__,__FILE__,TTF_GetError());
	} else
	{
		SDL_Log("width=%d height=%d\n",w,h);
	}
	}


	while (running)
	{
#if PRINTFPS
		SDL_Log("---------");
#endif

		// Calc past spent time
		//---
		
		double current_time = getTimeInSec();
		double frame_duration = current_time - old_time;
		double clamped_frame_duration = frame_duration;
#if PRINTFPS
		SDL_Log("frame_duration %fms", frame_duration*1000);
		SDL_Log("instant freq %fHz", 1.0/frame_duration);
#endif
		if (frame_duration > MAX_TIME)
		{
			clamped_frame_duration = MAX_TIME;
		} else
		if (frame_duration < MIN_TIME)
		{
			double ms_to_wait = (MIN_TIME - frame_duration) * 1000.0;
#if PRINTFPS
			SDL_Log("sleep for %fms",ms_to_wait);
#endif

			// minimum guaranteed sleep time is 9/10ms, SDL knows this but with it we sleep ~0.1ms more (on the machine of the author)
			if (ms_to_wait>=10)
			{
				// sleep for >=10ms
				SDL_Delay(static_cast<int>(ms_to_wait));

				// busy waiting for <1ms
				double start=getTimeInSec(),i;
				int it=0;
				ms_to_wait = std::modf(ms_to_wait,&i);
				// SDL_Log("b.w. %f",ms_to_wait);
				while ((getTimeInSec()-start)*1000.0 < ms_to_wait)
				{
					// it++;
				};
				// SDL_Log("b.w. for %d iterations",it);
			} else
			{
				//SDL_Delay(ms_to_wait); // try youtself uncommenting whis line and commenting the b.w.
				// busy waiting for max 10ms
				while ((getTimeInSec()-current_time)*1000.0 < ms_to_wait){};
			}
			current_time = getTimeInSec();
			double real_fd = current_time - old_time;
#if PRINTFPS
			SDL_Log("real_fd %fms", real_fd*1000);
			SDL_Log("real instant freq %fHz", 1.0/real_fd);
#endif
			clamped_frame_duration = MIN_TIME;
		}

		old_time = current_time;
		time_accumulator += clamped_frame_duration;
#if PRINTFPS
		SDL_Log("clamped %fms", clamped_frame_duration*1000);
		SDL_Log("time_accumulator %fms", time_accumulator*1000);
#endif

		n_steps = static_cast<int>(time_accumulator/FIXED_TIMESTEP);
#if PRINTFPS
		SDL_Log("n_steps %d", n_steps);
#endif


		int ww,wh;
		SDL_GetWindowSize(Environment::window,
		                  &ww,
		                  &wh);


		// Get Keymap state
		//---
		int nk;
		const Uint8* keys = SDL_GetKeyboardState(&nk);

		// Event management
		//---
		rotating = false;
		while (SDL_PollEvent(&event))
		{
			switch ( event.type )
			{
			case SDL_WINDOWEVENT:
				switch (event.window.event)
				{
				case SDL_WINDOWEVENT_CLOSE:
					SDL_Log("Window %d closed", event.window.windowID);
					running=false;
					break;
				default:
					break;
				}
				break;
			case SDL_MOUSEBUTTONUP:
			{
				// mouse_pos.setXY(event.button.x, wh - event.button.y);
				tG::UI::MouseButton which;
				switch (event.button.button)
				{
				case SDL_BUTTON_LEFT:
					which = tG::UI::LEFT_MB;
					break;
				case SDL_BUTTON_RIGHT:
					which = tG::UI::RIGHT_MB;
					break;
				case SDL_BUTTON_MIDDLE:
					which = tG::UI::MIDDLE_MB;
					break;
				case SDL_BUTTON_X1:
					which = tG::UI::_4_MB;
					break;
				case SDL_BUTTON_X2:
					which = tG::UI::_5_MB;
					break;
				default:
					assert(false);
					break;
				}
				if (tG::UI::mouseButtonInGUI(mouse_pos, which, true))
				{
					break;
				}
				scaling = false;
				rotating = false;
				translating = false;
				break;
			}
			case SDL_MOUSEBUTTONDOWN:
			{
				// mouse_pos.setXY(event.button.x, wh - event.button.y);

				tG::UI::MouseButton which;
				switch (event.button.button)
				{
				case SDL_BUTTON_LEFT:
					which = tG::UI::LEFT_MB;
					break;
				case SDL_BUTTON_RIGHT:
					which = tG::UI::RIGHT_MB;
					break;
				case SDL_BUTTON_MIDDLE:
					which = tG::UI::MIDDLE_MB;
					break;
				case SDL_BUTTON_X1:
					which = tG::UI::_4_MB;
					break;
				case SDL_BUTTON_X2:
					which = tG::UI::_5_MB;
					break;
				default:
					assert(false);
					break;
				}
				if (tG::UI::mouseButtonInGUI(mouse_pos, which, false))
				{
					break;
				}

				if (event.motion.state & SDL_BUTTON_LMASK &&
					keys[SDL_SCANCODE_LCTRL] &&
					!keys[SDL_SCANCODE_S] &&
					!keys[SDL_SCANCODE_R])
				{
					if (nearest_bone!=nullptr)
					{
						if (Bone::selected==nearest_bone)
						{
							animation_timeline.selectBone(nullptr);
						} else
						{
							animation_timeline.selectBone(nearest_bone);
						}
					}
				}

				if (event.motion.state & SDL_BUTTON_LMASK &&
					keys[SDL_SCANCODE_S] &&
					!keys[SDL_SCANCODE_R] &&
					!keys[SDL_SCANCODE_T] &&
					!keys[SDL_SCANCODE_LCTRL] &&
					Bone::selected!=nullptr)
				{
					std::cout<<"scaling started"<<std::endl;
					scaling = true;
					float t;
					TGMath::f::Vec2 pol;
					TGMath::f::distFromLine(mouse_pos, Bone::selected->v0, Bone::selected->v1, &pol, &t);
					scale_V = Bone::selected->v1 - pol;
					scale_pos_dir = (Bone::selected->v1 - Bone::selected->v0)*TGMath::f::sign(Bone::selected->properties.l);
					/**
					 * From now on v1 must be updated relatively to pol
					 * v1 = pol+scale_V; (i.e. calculate l as |v1-v2|)
					 **/
				} else
				if (event.motion.state & SDL_BUTTON_LMASK &&
					keys[SDL_SCANCODE_T] &&
					!keys[SDL_SCANCODE_S] &&
					!keys[SDL_SCANCODE_R] &&
					!keys[SDL_SCANCODE_LCTRL] &&
					Bone::selected!=nullptr)
				{
					translate_V = Bone::selected->v0 - mouse_pos;

					std::cout<<"translation started"<<std::endl;
					translate_V.print(std::cout);
					std::cout<<std::endl;
					translating = true;
				}

				if (event.motion.state & SDL_BUTTON_LMASK &&
					keys[SDL_SCANCODE_R] &&
					!keys[SDL_SCANCODE_S] &&
					!keys[SDL_SCANCODE_T] &&
					!keys[SDL_SCANCODE_LCTRL] &&
					Bone::selected!=nullptr)
				{
					std::cout<<"rotation started"<<std::endl;
					rotating = true;
				}
				break;
			}
			case SDL_MOUSEMOTION:
			{
				mouse_pos.setXY(event.motion.x, wh - event.motion.y);

				if (tG::UI::mouseMovedInGUI(mouse_pos, TGMath::i::Vec2(event.motion.xrel,event.motion.yrel)))
				{
					break;
				}

				if (event.motion.state & SDL_BUTTON_LMASK &&
					!keys[SDL_SCANCODE_LCTRL] &&
					!keys[SDL_SCANCODE_R] &&
					!keys[SDL_SCANCODE_T] &&
					!keys[SDL_SCANCODE_S])
					// Temporary move
					//---
				{
					bone0.properties.x = event.button.x;
					bone0.properties.y = wh - event.button.y;
				} else
				if (event.motion.state & SDL_BUTTON_LMASK &&
					keys[SDL_SCANCODE_R] &&
					!keys[SDL_SCANCODE_S] &&
					!keys[SDL_SCANCODE_T] &&
					!keys[SDL_SCANCODE_LCTRL] &&
					selected_keyframe!=nullptr &&
					Bone::selected!=nullptr)
					// Rotate
					//---
				{
					TGMath::i::Vec2 bone, motion(event.motion.xrel, -event.motion.yrel);
					bone.x = Bone::selected->v0.x;
					bone.y = Bone::selected->v0.y;
					selected_keyframe->properties.a += (mouse_pos-motion-bone).getAngleDeg(mouse_pos-bone);
					view_to_update = true;
				} else
				if (event.motion.state & SDL_BUTTON_LMASK &&
					keys[SDL_SCANCODE_S] &&
					!keys[SDL_SCANCODE_R] &&
					!keys[SDL_SCANCODE_T] &&
					!keys[SDL_SCANCODE_LCTRL] &&
					selected_keyframe!=nullptr &&
					Bone::selected!=nullptr)
					// Scale
					//---
				{
					TGMath::i::Vec2 motion(event.motion.xrel, -event.motion.yrel);
					float t;
					TGMath::f::distFromLine(mouse_pos, Bone::selected->v0, Bone::selected->v1, &pol, &t);
					TGMath::f::Vec2 new_v1 = pol+scale_V;
					const TGMath::f::Vec2& v0 = Bone::selected->v0;

					float sign_l = TGMath::f::sign(selected_keyframe->properties.l);
					selected_keyframe->properties.l = (new_v1-v0).getCos(scale_pos_dir) * (new_v1 -v0).getModule();
					selected_keyframe->properties.l = selected_keyframe->properties.l==0?sign_l:selected_keyframe->properties.l;
					scale_mode = scaling = true;
					view_to_update = true;
				} else
				if (event.motion.state & SDL_BUTTON_LMASK &&
					keys[SDL_SCANCODE_T] &&
					!keys[SDL_SCANCODE_S] &&
					!keys[SDL_SCANCODE_R] &&
					!keys[SDL_SCANCODE_LCTRL] &&
					selected_keyframe!=nullptr &&
					Bone::selected!=nullptr)
					// Translate
					//---
				{
					const TGMath::f::Vec2& p_v0 = Bone::selected->parent==nullptr ?
					                               TGMath::f::Vec2(0,0) :
					                               Bone::selected->parent->v0;
					const TGMath::f::Vec2& p_v1 = Bone::selected->parent==nullptr ?
					                               TGMath::f::Vec2(0,0) :
					                               Bone::selected->parent->v1;

					float p_a = Bone::selected->parent==nullptr ?
					             0 :
					             (p_v1-p_v0).getAngleRad(TGMath::f::Vec2(1,0));

					TGMath::f::Vec2 new_pos = (mouse_pos + translate_V - p_v1);
					new_pos.rotateRad(p_a);
					selected_keyframe->properties.x = new_pos.x;
					selected_keyframe->properties.y = new_pos.y;
					translate_mode = translating = true;
					view_to_update = true;
				}

				if (keys[SDL_SCANCODE_S] &&
					!keys[SDL_SCANCODE_R] &&
					!keys[SDL_SCANCODE_T] &&
					!keys[SDL_SCANCODE_LCTRL] &&
					Bone::selected!=nullptr &&
					!scaling)
				{
					// update pol to render it
					scale_mode = true;
					TGMath::f::distFromLine(mouse_pos, Bone::selected->v0, Bone::selected->v1, &pol, nullptr);
				}

				if (keys[SDL_SCANCODE_T] &&
					!keys[SDL_SCANCODE_R] &&
					!keys[SDL_SCANCODE_S] &&
					!keys[SDL_SCANCODE_LCTRL] &&
					Bone::selected!=nullptr &&
					!translating)
				{
					// update translate_V to render it
					translate_V = Bone::selected->v0 - mouse_pos;
				}
				prev_mouse_p = mouse_pos;
				break;
			}
			case SDL_KEYUP:
			{
				if (tG::UI::keyReleased(event.key.keysym.sym, keys))
					break;
		 		switch (event.key.keysym.sym)
		 		{
		 			case SDLK_r:
		 				rotate_mode = rotating = false;
		 			break;
		 			case SDLK_s:
		 				scale_mode = scaling = false;
		 			break;
		 			case SDLK_t:
		 				translate_mode = translating = false;
		 			break;
		 			default:
		 			break;
		 		}
				break;
			}
			case SDL_KEYDOWN:
			{
				if (tG::UI::keyPressed(event.key.keysym.sym, keys))
					break;
		 		switch (event.key.keysym.sym)
		 		{
		 			case SDLK_e:
		 				edit_mode = !edit_mode;
		 				break;
		 			case SDLK_n:
		 				Bone::selectNext();
		 				break;
		 			case SDLK_p:
			 			Bone::selectPrev();
		 				break;

		 			case SDLK_s:
		 				if (Bone::selected!=nullptr && !scale_mode)
		 				{
							TGMath::f::distFromLine(mouse_pos, Bone::selected->v0, Bone::selected->v1, &pol, nullptr);
						}
		 				break;

		 			case SDLK_t:
		 				if (Bone::selected!=nullptr && !translate_mode)
		 				{
							// update translate_V to render it
							translate_V = Bone::selected->v0 - mouse_pos;
						}
		 				break;

		 		// 	case SDLK_r:
		 		// 	{
			 	// 		Uint32 status; // the mouse buttons status bit mask, which can be tested using the SDL_BUTTON(X) macros (where X is generally 1 for the left, 2 for middle, 3 for the right button)
					// 	TGMath::i::Vec2 mouse_pos;
					// 	status = SDL_GetMouseState( &mouse_pos.x,     // the x coordinate of the mouse cursor position relative to the focus window
					// 	                            &mouse_pos.y);  // the x coordinate of the mouse cursor position relative to the focus window
					// 	prev_mouse_p = mouse_pos;
					// }
		 		// 		break;
		 			case SDLK_UP:
		 				if (Bone::selected!=nullptr)
				 			Bone::selected->lenInc();
		 				break;
		 			case SDLK_DOWN:
		 				if (Bone::selected!=nullptr)
		 				{
				 			Bone::selected->lenDec();
				 		}
		 				break;
		 			case SDLK_LEFT:
		 				if (Bone::selected!=nullptr)
		 				{
				 			Bone::selected->angDec();
			 			}
		 				break;
		 			case SDLK_RIGHT:
		 				if (Bone::selected!=nullptr)
			 			{
			 				Bone::selected->angInc();
			 			}
		 				break;
		 			// case SDLK_SPACE:
		 			// 	if (!animating)
		 			// 	{
						// 	sk.startAnimation(selected_animation_name, game_time);
		 			// 		animating = true;
		 			// 		selected_keyframe = nullptr;
						// } else
						// {
						// 	animating = false;
						// 	sk.stopAnimation();
						// }
		 			// 	break;
		 			case SDLK_ESCAPE:
			 			Bone::selectNone();
		 				break;
		 			case SDLK_0:
		 				selected_animation_name = "test_0delay";
		 				std::cout<<"animation \""<<selected_animation_name<<"\" selected"<<std::endl;
		 				break;
		 			case SDLK_1:
		 				selected_animation_name = "test_delay";
		 				std::cout<<"animation \""<<selected_animation_name<<"\" selected"<<std::endl;
		 				break;
		 			default:
		 				break;
		 		}
		 	}
			default:
				break;
			}
		}


		// Fixed time-step update
		//---

		if (n_steps > 0)
		{
			time_accumulator -= n_steps*FIXED_TIMESTEP;
#if PRINTFPS
			SDL_Log("over %fms", time_accumulator*1000);
#endif

			for (int s=0; s!=n_steps; s++)
			{
				/**
				 * for each element
				 * previous_state = current_state
				 * fixedTSUpdate(game_time, FIXED_TIMESTEP)
				 **/

				game_time += FIXED_TIMESTEP;
			}
		}
#if PRINTFPS
		SDL_Log("time_accumulator %fms", time_accumulator*1000);
#endif

		alpha = time_accumulator / FIXED_TIMESTEP;


		// Time dependant update
		//---

		/**
		 * for each element
		 * update(game_time, n_steps*FIXED_TIMESTEP)
		 **/
		animation_timeline.update(game_time);
		// Bone::animateAll(game_time);
		if (view_to_update)
		{
			animation_timeline.getSkeleton()->animateAtRelativeTime(selected_keyframe->time);
			view_to_update = false;
		}

		Uint32 status; // the mouse buttons status bit mask, which can be tested using the SDL_BUTTON(X) macros (where X is generally 1 for the left, 2 for middle, 3 for the right button)
		TGMath::f::Vec2 nearest_point;
		TGMath::i::Vec2 mouse_pos;
		status = SDL_GetMouseState( &mouse_pos.x,     // the x coordinate of the mouse cursor position relative to the focus window
		                            &mouse_pos.y);  // the x coordinate of the mouse cursor position relative to the focus window
		// status & SDL_BUTTON(1)
		mouse_pos.y = wh-mouse_pos.y;

		if (Bone::selected!=nullptr)
		{
			Bone::selected->color_id = Bone::SELECTED;
		}

		if (!keys[SDL_SCANCODE_R] &&
			!keys[SDL_SCANCODE_S] &&
			!keys[SDL_SCANCODE_T] &&
			keys[SDL_SCANCODE_LCTRL])
		{
			Bone* new_nearest_bone = Bone::getNearestBone(mouse_pos, &nearest_point, (float*)nullptr);
			if (new_nearest_bone!=nullptr)
			{
				if (nearest_bone!=nullptr)
				{
					nearest_bone->color_id = nearest_bone!=Bone::selected ?
					                          Bone::DEFAULT :
					                          Bone::SELECTED;
				}
				nearest_bone = new_nearest_bone;
				if (nearest_bone!=Bone::selected)
				{
					nearest_bone->color_id = Bone::CANDIDATE;
				}
			}
			if (nearest_bone!=nullptr)
			{
				if (keys[SDL_SCANCODE_LCTRL])
				{
					if (nearest_bone!=Bone::selected)
					{
						nearest_bone->color_id = Bone::CANDIDATE;
					}
				} else
				{
					if (nearest_bone!=Bone::selected)
					{
						nearest_bone->color_id = Bone::DEFAULT;
					} else
					{
						nearest_bone->color_id = Bone::SELECTED;
					}
				}
			}
		} else
		{
			if (nearest_bone!=nullptr)
			{
				if (nearest_bone!=Bone::selected)
				{
					nearest_bone->color_id = Bone::DEFAULT;
				} else
				{
					nearest_bone->color_id = Bone::SELECTED;
				}
				nearest_bone = nullptr;
			}
		}


		// Smooth
		//---

		/**
		 * for each element
		 * current_state = current_state * alpha + previous_state * (1.0 - alpha)
		 */


		// Render
		//---

		// set the color to draw with
		SDL_SetRenderDrawColor(
							Environment::renderer,    // SDL_Renderer* renderer: the renderer to affect
							64,          // Uint8 r: Red
							64,          // Uint8 g: Green
							64,          // Uint8 b: Blue
							255);        // Uint8 a: Alpha
		// Clear the entire screen to our selected color.
		SDL_RenderClear(Environment::renderer);

		// <Render all the elements here>
		// set the color to draw with
		SDL_SetRenderDrawColor(
							Environment::renderer,   // SDL_Renderer* renderer: the renderer to affect
							200,        // Uint8 r: Red
							20,        // Uint8 g: Green
							20,        // Uint8 b: Blue
							255);       // Uint8 a: Alpha
		bone0.drawTree(Environment::renderer);
		
		if (nearest_bone!=nullptr)
		{
			SDL_SetRenderDrawColor(
								Environment::renderer,   // SDL_Renderer* renderer: the renderer to affect
								100,        // Uint8 r: Red
								100,        // Uint8 g: Green
								100,        // Uint8 b: Blue
								255);       // Uint8 a: Alpha
			SDL_RenderDrawLine(Environment::renderer,
			                   mouse_pos.x, wh-mouse_pos.y,
			                   nearest_point.x, wh-nearest_point.y);
			Font::renderText(nearest_bone->name, Font::getFont(Font("./fonts/Inconsolata/Inconsolata-Regular.ttf", 12)), (nearest_bone->v0.x+nearest_bone->v1.x)/2, (nearest_bone->v0.y+nearest_bone->v1.y)/2);
		}
		if (Bone::selected!=nullptr &&
			keys[SDL_SCANCODE_R] &&
			!keys[SDL_SCANCODE_S] &&
			!keys[SDL_SCANCODE_T] &&
			!keys[SDL_SCANCODE_LCTRL])
		{
			SDL_SetRenderDrawColor(
								Environment::renderer,   // SDL_Renderer* renderer: the renderer to affect
								100,        // Uint8 r: Red
								100,        // Uint8 g: Green
								100,        // Uint8 b: Blue
								255);       // Uint8 a: Alpha
			SDL_RenderDrawLine(Environment::renderer,
			                   mouse_pos.x, wh-mouse_pos.y,
			                   Bone::selected->v0.x, wh-Bone::selected->v0.y);
			Font::renderText(tG::ms<<Bone::selected->properties.a<<"°", Font::getFont(Font("./fonts/Inconsolata/Inconsolata-Regular.ttf", 12)), (mouse_pos.x+ Bone::selected->v0.x)/2, (mouse_pos.y+Bone::selected->v0.y)/2);
		}
		if (Bone::selected!=nullptr &&
			keys[SDL_SCANCODE_S] &&
			!keys[SDL_SCANCODE_R] &&
			!keys[SDL_SCANCODE_T] &&
			!keys[SDL_SCANCODE_LCTRL])
		{
			SDL_SetRenderDrawColor(
								Environment::renderer,   // SDL_Renderer* renderer: the renderer to affect
								100,        // Uint8 r: Red
								100,        // Uint8 g: Green
								100,        // Uint8 b: Blue
								255);       // Uint8 a: Alpha
			SDL_RenderDrawLine(Environment::renderer,
			                   Bone::selected->v1.x, wh-Bone::selected->v1.y,
			                   pol.x, wh-pol.y);
			SDL_RenderDrawLine(Environment::renderer,
			                   mouse_pos.x, wh-mouse_pos.y,
			                   pol.x, wh-pol.y);

			Font::renderText(tG::ms<<Bone::selected->properties.l, Font::getFont(Font("./fonts/Inconsolata/Inconsolata-Regular.ttf", 12)), (mouse_pos.x+pol.x)/2, (mouse_pos.y+pol.y)/2);
		}
		if (Bone::selected!=nullptr &&
			keys[SDL_SCANCODE_T] &&
			!keys[SDL_SCANCODE_R] &&
			!keys[SDL_SCANCODE_S] &&
			!keys[SDL_SCANCODE_LCTRL])
		{
			SDL_SetRenderDrawColor(
								Environment::renderer,   // SDL_Renderer* renderer: the renderer to affect
								100,        // Uint8 r: Red
								100,        // Uint8 g: Green
								100,        // Uint8 b: Blue
								255);       // Uint8 a: Alpha
			SDL_RenderDrawLine(Environment::renderer,
			                   mouse_pos.x, wh - mouse_pos.y,
			                   mouse_pos.x + translate_V.x, wh - (mouse_pos.y + translate_V.y));
			SDL_RenderDrawLine(Environment::renderer,
			                   mouse_pos.x, wh - mouse_pos.y,
			                   Bone::selected->v1.x, wh - Bone::selected->v1.y);
			Font::renderText(tG::ms<<Bone::selected->properties.x<<", "<<Bone::selected->properties.y, Font::getFont(Font("./fonts/Inconsolata/Inconsolata-Regular.ttf", 12)), (mouse_pos.x+(Bone::selected->v0.x+Bone::selected->v1.x)/2)/2, (mouse_pos.y+(Bone::selected->v0.y+Bone::selected->v1.y)/2)/2);
		}

		// Render UI
		//---
		tG::UI::render();
		
		// update the screen with the performed rendering
		SDL_RenderPresent(Environment::renderer);
	}


	// Exit
	//---

	// Destroy renderer
	SDL_DestroyRenderer(Environment::renderer);

	// Close and destroy the window
	SDL_DestroyWindow(Environment::window);

	// Quit SDL
	SDL_Quit();

	// Close the font
	TTF_CloseFont(font);

	// Quit SDL_ttf
	if (TTF_WasInit())
	{
		TTF_Quit();
	}
	return 0;
}
